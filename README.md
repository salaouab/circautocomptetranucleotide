# Circular Auto Complementary Tetranucleotide Sets #

### What is this repository for? ###

This project is developed in the context of college project.  The aim is to seach for circular auto complementary tetranucleotide sets. The key is that **a circular tetranucleotide set does not admit a cycle in its directed graph representation**.
Giving the tetranucleotide ACGT one can have these edges : {***A->CGT*** , ***AC->GT*** ***ACG->T*** }. So the idea is to perform all combination of tetranucleotide sets to discovery sets that fits the purpose