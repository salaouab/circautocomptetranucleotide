package fr.unistra.view.chart;

import java.util.ArrayList;

import org.jfree.chart.labels.CategoryToolTipGenerator;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.data.category.CategoryDataset;

public class TimeLabelTooltip {

	ArrayList<String>	tooltipValues	= new ArrayList<String>();

	public void addToolTipValue(String value) {
		tooltipValues.add(value);
	}

	public class TimeToolTipGenerator implements CategoryToolTipGenerator {
		@Override
		public String generateToolTip(CategoryDataset dataset, int row, int column) {
			String timePrefix = "Time " + (column + 1) + " = ";
			try {
				return timePrefix + tooltipValues.get(column);
			} catch (Exception exception) {
				return timePrefix + dataset.getValue(row, column).intValue() + " h";
			}
		}
	}

	public class TimeLabelGenerator extends StandardCategoryItemLabelGenerator {
		private static final long	serialVersionUID	= 1L;

		@Override
		public String generateLabel(CategoryDataset dataset, int row, int column) {
			try {
				return tooltipValues.get(column);
			} catch (Exception exception) {
				return dataset.getValue(row, column).intValue() + " h";
			}
		}
	}
}