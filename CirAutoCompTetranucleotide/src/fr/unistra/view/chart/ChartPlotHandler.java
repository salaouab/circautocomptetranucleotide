package fr.unistra.view.chart;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;

import fr.unistra.view.chart.TimeLabelTooltip.TimeLabelGenerator;
import fr.unistra.view.chart.TimeLabelTooltip.TimeToolTipGenerator;

public class ChartPlotHandler {
	private JFreeChart	      chart;
	private LineRenderHandler	lineRenderer;
	private BarRenderHandler	barRenderer;
	private CategoryPlot	  plot;

	public ChartPlotHandler(CategoryDataset dataset, String chartTitle, String domainLabel, String rangeLabel) {
		// create the chart...
		chart = ChartFactory.createLineChart3D(chartTitle, domainLabel, rangeLabel, dataset, PlotOrientation.VERTICAL, false, true, false);
		chart.getTitle().setPaint(Color.WHITE);
		chart.getTitle().setFont(new Font("Tahoma", Font.BOLD, 16));
		chart.setBackgroundPaint(Color.DARK_GRAY);
		plot = (CategoryPlot) chart.getPlot();
		plot.setBackgroundPaint(Color.lightGray);
		plot.setRangeGridlinePaint(Color.black);
		plot.setOutlinePaint(Color.cyan);

		// customise the range axis...
		plot.getRangeAxis().setLabelPaint(Color.cyan);
		plot.getRangeAxis().setTickLabelPaint(Color.white);
		plot.getRangeAxis().setTickMarkPaint(Color.white);
		plot.getRangeAxis().setAxisLinePaint(Color.cyan);
		plot.setRangeGridlinesVisible(false);
		// customise the domain axis...
		plot.getDomainAxis().setLabelPaint(Color.cyan);
		plot.getDomainAxis().setTickLabelPaint(Color.white);
		plot.getDomainAxis().setTickMarkPaint(Color.white);
		plot.getDomainAxis().setAxisLinePaint(Color.cyan);
		plot.setDomainGridlinesVisible(false);
		barRenderer = new BarRenderHandler();
		lineRenderer = new LineRenderHandler();
		plot.setRenderer(lineRenderer);
		showItemLabel(false);
	}

	public void switchLineChart() {
		plot.setRenderer(lineRenderer);
	}

	public void switchBarChart() {
		plot.setRenderer(barRenderer);
	}

	public void showItemLabel(boolean state) {
		barRenderer.setBaseItemLabelsVisible(state);
		lineRenderer.setBaseItemLabelsVisible(state);
	}

	public void showGridLine(boolean state) {
		plot.setRangeGridlinesVisible(state);
		plot.setDomainGridlinesVisible(state);
	}

	public void showXLine(boolean state) {
		plot.setDomainGridlinesVisible(state);
	}

	public void showYLine(boolean state) {
		plot.setRangeGridlinesVisible(state);
	}

	public void setToolTipGenerator(TimeToolTipGenerator tooltipGenerator) {
		barRenderer.setBaseToolTipGenerator(tooltipGenerator);
		lineRenderer.setBaseToolTipGenerator(tooltipGenerator);
	}

	public void setLabelGenerator(TimeLabelGenerator labelGenerator) {
		barRenderer.setBaseItemLabelGenerator(labelGenerator);
		lineRenderer.setBaseItemLabelGenerator(labelGenerator);
	}

	public JFreeChart getChart() {
		return chart;
	}

	public void saveAsJPEG(File file, int width, int height) {
		try {
			ChartUtilities.saveChartAsJPEG(file, chart, width, height);
		} catch (IOException exception) {}
	}

	public void saveAsPNG(File file, int width, int height) {
		try {
			ChartUtilities.saveChartAsPNG(file, chart, width, height);
		} catch (IOException exception) {}
	}
}
