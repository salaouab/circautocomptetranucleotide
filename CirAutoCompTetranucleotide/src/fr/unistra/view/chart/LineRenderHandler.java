package fr.unistra.view.chart;

import static fr.unistra.view.utils.viewUtils.componentFont12;

import java.awt.Color;
import java.awt.Paint;

import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.renderer.category.LineRenderer3D;

class LineRenderHandler extends LineRenderer3D {
	private static final long	serialVersionUID	= 1L;

	public LineRenderHandler() {
		setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		setBaseItemLabelsVisible(true);
		setBaseToolTipGenerator(new ToolTipHandler());
		setBaseItemLabelFont(componentFont12);
		setBaseItemLabelPaint(Color.red);
		setSeriesPaint(0, Color.orange);
	}

	@Override
	public Paint getItemOutlinePaint(int row, int column) {
		// return Color.black;
		return Color.black;
	}
}