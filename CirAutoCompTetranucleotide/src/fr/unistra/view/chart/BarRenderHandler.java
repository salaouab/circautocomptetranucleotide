package fr.unistra.view.chart;

import static fr.unistra.view.utils.viewUtils.componentFont12;

import java.awt.Color;
import java.awt.Paint;

import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.ui.TextAnchor;

public class BarRenderHandler extends BarRenderer3D {
	private static final long	serialVersionUID	= 1L;
	Color	                  color1	         = new Color(102, 178, 255);
	Color	                  color2	         = new Color(255, 128, 0);

	public BarRenderHandler() {
		setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		setBaseItemLabelPaint(Color.red);
		setBaseItemLabelsVisible(true);
		setBaseItemLabelFont(componentFont12);
		setDrawBarOutline(false);

		setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_CENTER));
		setItemLabelAnchorOffset(10);
		setDrawBarOutline(true);
		setBaseToolTipGenerator(new ToolTipHandler());
	}

	@Override
	public Paint getItemPaint(int row, int column) {
		// return column % 2 == 0 ? color1 : color2;
		// return column % 2 == 0 ? Color.yellow : color2;
		return column % 2 == 0 ? color1 : Color.yellow;
	}

	@Override
	public Paint getItemOutlinePaint(int row, int column) {
		return Color.black;
	}
}