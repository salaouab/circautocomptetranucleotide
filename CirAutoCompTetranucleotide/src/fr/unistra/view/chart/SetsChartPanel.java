package fr.unistra.view.chart;

import org.jfree.chart.ChartPanel;
import org.jfree.data.category.DefaultCategoryDataset;

import fr.unistra.utils.TaskObserver;

public class SetsChartPanel extends ChartPanel implements TaskObserver {
	private static final long	   serialVersionUID	= 1L;
	private String	               chartTitle	    = "Number of Circular Tetranucleotide Autocomplementary Sets By Set Lenght";
	private String	               domainLabel	    = "Set Lenght";
	private String	               rangeLabel	    = "Number of Sets";
	private DefaultCategoryDataset	dataset;
	private ChartPlotHandler	   setsChartPlot;

	public SetsChartPanel() {
		super(null);
		dataset = new DefaultCategoryDataset();
		// dataset = createDataset();
		setsChartPlot = new ChartPlotHandler(dataset, chartTitle, domainLabel, rangeLabel);

		setChart(setsChartPlot.getChart());
	}

	public ChartPlotHandler getSetsChartPlot() {
		return setsChartPlot;
	}

	@Override
	public void notifyResult(int currentSetLength, long numberOfSets, long timeInSecond, String timeFormatted) {
		dataset.addValue(numberOfSets, "Number Of Sets", currentSetLength + "");
	}

	@Override
	public void notifyStart(int currentSetLength, long numberOfTaskToPerform) {}

	@Override
	public void notifyProgress(long numberOfTaskPerformed) {}

	@Override
	public void notifyEnd() {}
}
