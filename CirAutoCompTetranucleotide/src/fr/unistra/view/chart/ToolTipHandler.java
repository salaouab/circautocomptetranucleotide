package fr.unistra.view.chart;

import static fr.unistra.utils.GraphAlgorithmConstants.NumberFormatter;

import org.jfree.chart.labels.CategoryToolTipGenerator;
import org.jfree.data.category.CategoryDataset;

public class ToolTipHandler implements CategoryToolTipGenerator {
	@Override
	public String generateToolTip(CategoryDataset dataset, int row, int column) {
		long val = dataset.getValue(row, column).intValue();
		return "Lenght " + (column + 1) + " = " + NumberFormatter.format(val) + " Sets";
	}
}