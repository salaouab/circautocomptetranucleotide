package fr.unistra.view.chart;

import org.jfree.chart.ChartPanel;
import org.jfree.data.category.DefaultCategoryDataset;

import fr.unistra.utils.TaskObserver;

public class TimeChartPanel extends ChartPanel implements TaskObserver {
	private static final long	   serialVersionUID	= 1L;
	private String	               chartTitle	    = "Incremental Processing Time By Set Lenght";
	private String	               domainLabel	    = "Set Lenght";
	private String	               rangeLabel	    = "Elapsed Time In Hour";
	private DefaultCategoryDataset	dataset;
	private TimeLabelTooltip	   timeLabelTooltip;
	private ChartPlotHandler	   timeChartPlot;

	public TimeChartPanel() {
		super(null);
		dataset = new DefaultCategoryDataset();
		timeChartPlot = new ChartPlotHandler(dataset, chartTitle, domainLabel, rangeLabel);
		timeLabelTooltip = new TimeLabelTooltip();
		timeChartPlot.setLabelGenerator(timeLabelTooltip.new TimeLabelGenerator());
		timeChartPlot.setToolTipGenerator(timeLabelTooltip.new TimeToolTipGenerator());

		setChart(timeChartPlot.getChart());
	}

	public ChartPlotHandler getTimeChartPlot() {
		return timeChartPlot;
	}

	@Override
	public void notifyResult(int currentSetLength, long numberOfSets, long timeInSecond, String timeFormatted) {
		timeLabelTooltip.addToolTipValue(timeFormatted);
		double timeInHours = timeInSecond / (double) 3600;
		dataset.addValue(timeInHours, "Elpased Time", currentSetLength + "");
	}

	@Override
	public void notifyStart(int currentSetLength, long numberOfTaskToPerform) {}

	@Override
	public void notifyProgress(long numberOfTaskPerformed) {}

	@Override
	public void notifyEnd() {}
}
