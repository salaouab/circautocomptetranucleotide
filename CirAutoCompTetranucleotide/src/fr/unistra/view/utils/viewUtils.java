package fr.unistra.view.utils;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JComponent;

/**
 * Some graphic constants
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class viewUtils {
	public final static Color	CYAN_COLOR	  = new Color(72, 209, 204);
	public static Font	      componentFont12	= new Font("SansSerif", Font.BOLD, 12);
	public static Font	      componentFont15	= new Font("SansSerif", Font.BOLD, 15);
	public static Color	      foregroundColor	= Color.WHITE;
	public static Color	      backgroundColor	= Color.DARK_GRAY;

	public static void initComponentProperties(JComponent component) {
		component.setForeground(foregroundColor);
		component.setBackground(backgroundColor);
		component.setFont(componentFont15);
	}

	public static void initComponentProperties2(JComponent component) {
		component.setForeground(foregroundColor);
		component.setBackground(backgroundColor);
		component.setFont(componentFont12);
	}

}
