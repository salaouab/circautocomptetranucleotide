package fr.unistra.view.utils;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

/**
 * Modal dialog showing some text input content information.
 * @author Abdoul-djawadou SALAOU
 */
@SuppressWarnings("serial")
public class InformationDialog extends JDialog {
	private JPanel	       iconPanel;
	private JTextPane	   textPane;
	private JLabel	       leftLabel;
	private JLabel	       centerLabel;
	private JLabel	       rightLabel;
	static final ImageIcon	unistraIcon	= new ImageIcon(InformationDialog.class.getResource("/image/unistra.png"));
	static final ImageIcon	appIcon	    = new ImageIcon(InformationDialog.class.getResource("/image/circularTetranuCode.png"));
	static final ImageIcon	icubeIcon	= new ImageIcon(InformationDialog.class.getResource("/image/icube.png"));
	static final ImageIcon	warningIcon	= new ImageIcon(InformationDialog.class.getResource("/image/warning.png"));

	/**
	 * Show dialog with html data support
	 * @param contentData message to print
	 * @param title the dialog title
	 * @param size size of the dialog
	 * @param parent dialog parent
	 */
	public InformationDialog(String contentData, String title, Dimension size, Component parent) {
		this(size, parent);
		textPane.setText(contentData);
		textPane.setCaretPosition(0);

		leftLabel.setIcon(warningIcon);
		centerLabel.setText(title);
		rightLabel.setIcon(warningIcon);
		centerLabel.setFont(new Font("sans serif", Font.BOLD, 28));
		centerLabel.setForeground(Color.BLACK);
		iconPanel.setBackground(new Color(72, 207, 204));

	}

	/**
	 * Show dialog with html data support
	 * @param contentData message to print
	 * @param size size of the dialog
	 * @param parent dialog parent
	 */
	public InformationDialog(String contentData, Dimension size, Component parent) {
		this(size, parent);
		textPane.setText(contentData);
		textPane.setCaretPosition(0);

		leftLabel.setIcon(unistraIcon);
		centerLabel.setIcon(appIcon);
		rightLabel.setIcon(icubeIcon);
		iconPanel.setBackground(Color.WHITE);

	}

	/**
	 * Show dialog with html data support
	 * @param contentData message to print
	 * @param title the dialog title
	 * @param size size of the dialog
	 * @param parent dialog parent
	 */
	private InformationDialog(Dimension size, Component parent) {
		setResizable(false);
		setUndecorated(true);
		setModal(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().setBackground(Color.WHITE);
		setBackground(Color.WHITE);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

		leftLabel = new JLabel();
		centerLabel = new JLabel();
		rightLabel = new JLabel();

		iconPanel = new JPanel();
		iconPanel.setBackground(Color.WHITE);
		iconPanel.setLayout(new BoxLayout(iconPanel, BoxLayout.X_AXIS));
		iconPanel.add(leftLabel);
		iconPanel.add(Box.createHorizontalGlue());
		iconPanel.add(centerLabel);
		iconPanel.add(Box.createHorizontalGlue());
		iconPanel.add(rightLabel);

		getContentPane().add(iconPanel);

		// Text pane area
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(new EmptyBorder(7, 9, 7, 9));
		scrollPane.setBackground(Color.WHITE);
		getContentPane().add(scrollPane);

		textPane = new JTextPane();
		textPane.setContentType("text/html");

		HTMLEditorKit kit = new HTMLEditorKit();
		textPane.setEditorKit(kit);
		URL cssFileURL = InformationDialog.class.getResource("/help/style.css");

		StyleSheet styleSheet = kit.getStyleSheet();
		styleSheet.importStyleSheet(cssFileURL);
		Document doc = kit.createDefaultDocument();
		textPane.setDocument(doc);

		textPane.setFont(new Font("Dialog", Font.PLAIN, 20));
		textPane.setEditable(false);
		textPane.setBackground(Color.WHITE);
		textPane.setDragEnabled(true);
		scrollPane.setViewportView(textPane);

		// ok button
		JButton boutonOK = new JButton("   OK   ");
		boutonOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		boutonOK.setAlignmentX(Component.CENTER_ALIGNMENT);
		getContentPane().add(boutonOK);
		setSize(size);
		setLocationRelativeTo(parent);
	}

}
