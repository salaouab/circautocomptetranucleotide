package fr.unistra.view.utils;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.LineBorder;

/**
 * Modal dialog for information showing, question asking,and input requirement.
 * @author Abdoul-djawadou SALAOU
 */
/**
 * @author salaou
 */
@SuppressWarnings("serial")
// Rename to General dialog
public class GeneralDialog extends JDialog {
	private JLabel	             titleLabel;
	private JButton	             buttonYes;
	private JButton	             buttonNo;
	private JTextArea	         messageTextArea;
	private JLabel	             iconLabel;
	private JTextField	         numericField;
	private static ImageIcon	 numericIcon;
	private static ImageIcon	 questionIcon;
	private static ImageIcon	 informationIcon;
	private boolean	             userChoice;
	/** Main dialog for information, question */
	private static GeneralDialog	infoQuestDialog	= new GeneralDialog();
	/** Input dialog, for avoiding latency when showed consecutively */
	private static GeneralDialog	inputDialog	    = new GeneralDialog();

	/**
	 * Show question pup dialog with question mark icon for YES or NO answering
	 * @param messageText : Message to show the user
	 * @param title : The dialog Title
	 * @param parent : Dialog parent; null value is also accepted if none
	 * @return Return true if the user clicked YES, false Otherwise
	 * @see #questionAdjustment(String, String, JComponent)
	 */
	public static boolean showQuestion(String messageText, String title, Component parent) {
		infoQuestDialog.questionAdjustment(messageText, title, parent);
		infoQuestDialog.pack();
		infoQuestDialog.setVisible(true);
		return infoQuestDialog.getUserChoice();
	}

	/**
	 * Show information pup dialog with I icon Ok comfirmation
	 * @param messageText : Message to show the user
	 * @param title : The dialog Title
	 * @param parent : Dialog parent; null value is also accepted if none
	 * @see #informationAdjustment(String, String, JComponent)
	 */
	public static void showInformation(String messageText, String title, Component parent) {
		infoQuestDialog.informationAdjustment(messageText, title, parent);
		infoQuestDialog.pack();
		infoQuestDialog.setVisible(true);
		return;
	}

	/**
	 * Show input pup dialog with Numeric icon for Submit or Cancel action
	 * @param messageText : Message to show the user
	 * @param parent : Dialog parent; null value is also accepted if none
	 * @return Return integer value represented the user input value, -1 if an error occure and 0
	 *         Cancel button was clicked
	 * @see #numericInputAdjustment(String, JComponent)
	 */
	public static int showNumericInput(String messageText, Component parent) {
		inputDialog.numericInputAdjustment(messageText, parent);
		inputDialog.pack();
		inputDialog.setVisible(true);
		return inputDialog.getUserNumericValue();
	}

	private GeneralDialog() {
		setResizable(false);
		setUndecorated(true);
		setModal(true);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0, 10));
		getContentPane().setBackground(Color.WHITE);
		setBackground(Color.WHITE);

		// Init icons
		questionIcon = new ImageIcon(getClass().getResource("/image/question.png"));
		informationIcon = new ImageIcon(getClass().getResource("/image/information.png"));
		numericIcon = new ImageIcon(getClass().getResource("/image/numeric.png"));

		// title label
		titleLabel = new JLabel();
		titleLabel.setOpaque(true);
		titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		// titleLabel.setBackground(Color.DARK_GRAY);
		// titleLabel.setForeground(new Color(72, 207, 204));
		titleLabel.setForeground(Color.DARK_GRAY);
		titleLabel.setBackground(new Color(72, 207, 204));
		titleLabel.setFont(new Font("sans serif", Font.BOLD, 20));
		titleLabel.setFocusable(false);
		getContentPane().add(titleLabel, BorderLayout.NORTH);

		messageTextArea = new JTextArea(2, 30);
		messageTextArea.setFont(new Font("Dialog", Font.PLAIN, 18));
		messageTextArea.setBackground(Color.WHITE);
		messageTextArea.setLineWrap(true);
		messageTextArea.setWrapStyleWord(true);
		messageTextArea.setEditable(false);
		messageTextArea.setFocusable(false);
		messageTextArea.setBorder(new LineBorder(Color.WHITE));

		numericField = new JTextField(30);
		numericField.setCaretColor(new Color(220, 20, 60));
		numericField.setHorizontalAlignment(SwingConstants.CENTER);
		numericField.setBackground(new Color(173, 216, 230));
		numericField.setFont(new Font("Dialog", Font.PLAIN, 18));

		iconLabel = new JLabel("   ");
		iconLabel.setBackground(Color.WHITE);
		iconLabel.setFocusable(false);
		// panel containing icon, message, input field
		JPanel mainPanel = new JPanel();
		mainPanel.add(iconLabel);
		mainPanel.add(messageTextArea);
		mainPanel.add(numericField);
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setFocusable(false);

		// add mainpanel to dialog
		getContentPane().add(mainPanel, BorderLayout.CENTER);

		buttonYes = new JButton("   Yes   ");
		buttonYes.setFont(new Font("SansSerif", Font.BOLD, 12));
		buttonYes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				userChoice = true;
			}
		});

		buttonNo = new JButton("   No   ");
		buttonNo.setFont(new Font("SansSerif", Font.BOLD, 12));
		buttonNo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				userChoice = false;
			}
		});

		// panel holding buttons
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 30, 10));
		buttonPanel.add(Box.createHorizontalStrut(15));
		buttonPanel.add(buttonYes);
		buttonPanel.add(buttonNo);
		buttonPanel.setBackground(Color.WHITE);
		buttonPanel.setFocusable(false);

		// add buttonPanel to Dialog
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);

	}

	/**
	 * Return the numeric field integer parsed value.
	 * @return Return integer value represented the user input value, -1 if an error occure and 0
	 *         Cancel button was clicked
	 */
	private int getUserNumericValue() {
		if (!userChoice)
			return 0;
		int userValue;
		try {
			userValue = Integer.parseInt(numericField.getText());
		} catch (Exception e) {
			userValue = -1;
		}
		return userValue;
	}

	/**
	 * Adust the dialog for question with question mark icon and YES or NO buttons
	 * @param messageText : Message to show the user
	 * @param title : The dialog Title
	 * @param parent : Dialog parent; null value is also accepted if none
	 */
	private void questionAdjustment(String messageText, String title, Component parent) {
		iconLabel.setIcon(questionIcon);
		numericField.setVisible(false);
		messageTextArea.setVisible(true);
		buttonNo.setVisible(true);

		messageTextArea.setText(messageText);
		titleLabel.setText(title);
		buttonYes.setText("  Yes  ");
		buttonNo.setText("   No  ");
		setLocationRelativeTo(parent);
		pack();
	}

	/**
	 * Adjust the dialog for information with I icon and Ok comfirmation button
	 * @param messageText : Message to show the user
	 * @param title : The dialog Title
	 * @param parent : Dialog parent; null value is also accepted if none
	 */
	private void informationAdjustment(String messageText, String title, Component parent) {
		iconLabel.setIcon(informationIcon);
		numericField.setVisible(false);
		messageTextArea.setVisible(true);
		buttonNo.setVisible(false);

		messageTextArea.setText(messageText);
		titleLabel.setText(title);
		buttonYes.setText("  OK  ");
		setLocationRelativeTo(parent);
		pack();
	}

	/**
	 * Adjust the dialog for input with Numeric icon and Submit or Cancel buttons
	 * @param messageText : Message to show the user
	 * @param parent : Dialog parent; null value is also accepted if none
	 */
	private void numericInputAdjustment(String messageText, Component parent) {
		iconLabel.setIcon(numericIcon);
		numericField.setVisible(true);
		messageTextArea.setVisible(false);
		buttonNo.setVisible(true);

		titleLabel.setText(messageText);
		buttonYes.setText("Submit");
		buttonNo.setText("Cancel");
		numericField.requestFocusInWindow();
		setLocationRelativeTo(parent);
		pack();
	}

	public boolean getUserChoice() {
		return userChoice;
	}

	public static void main(String[] args) {
		GeneralDialog.showQuestion("Bien tout vien de faire pas mal de chose alors on dit quoi Bien ", "Concern", null);
		System.out.println("bien connu");
	}
}
