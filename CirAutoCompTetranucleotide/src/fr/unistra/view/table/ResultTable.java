package fr.unistra.view.table;

import static fr.unistra.utils.GraphAlgorithmConstants.COLUMN_NAME;
import static fr.unistra.utils.GraphAlgorithmConstants.NumberFormatter;
import static fr.unistra.view.utils.viewUtils.*;

import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import fr.unistra.utils.TaskObserver;

/**
 * Table displaying result data and elapsed time
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class ResultTable extends JScrollPane implements TaskObserver {
	private static final long	serialVersionUID	= 1L;
	private JTable	          resultTable;
	private DefaultTableModel	tableModel;

	public ResultTable() {
		tableModel = new DefaultTableModel() {
			private static final long	serialVersionUID	= 1L;

			@Override
			public boolean isCellEditable(int i, int i1) {
				return false;
			}
		};
		tableModel.setColumnIdentifiers(COLUMN_NAME);
		resultTable = new JTable(tableModel);
		MyTableCellRenderer tableCellRenderer = new MyTableCellRenderer();
		resultTable.setDefaultRenderer(Object.class, tableCellRenderer);
		MyTableCellRenderer.setHederFont(resultTable);
		resultTable.setFillsViewportHeight(true);
		resultTable.setBackground(backgroundColor);
		getViewport().add(resultTable);
		LineBorder lineBorder = new LineBorder(CYAN_COLOR, 4, true);

		TitledBorder titledborder = new TitledBorder(lineBorder, "Result Table", TitledBorder.LEADING, TitledBorder.TOP, componentFont15, foregroundColor);
		setBorder(titledborder);
		setBackground(backgroundColor);
		// setMaximumSize(new Dimension(Integer.MAX_VALUE, 315));
		setMaximumSize(new Dimension(600, 314));
		setPreferredSize(new Dimension(600, 314));
	}

	public void addRow(String[] rowTab) {
		tableModel.addRow(rowTab);
	}

	public void addRow(Object val1, Object val2, Object val3) {
		Object[] tab = { val1, val2, val3 };
		tableModel.addRow(tab);
	}

	public JTable getResultTable() {
		return resultTable;
	}

	public void saveTableToFile(File file, String separator, boolean includeTimeColumn) {
		PrintWriter printWriter = null;
		try {
			printWriter = new PrintWriter(file, "utf-8");
			// Write Table header
			printWriter.print(COLUMN_NAME[0] + separator + COLUMN_NAME[1]);
			if (includeTimeColumn)
				printWriter.println(separator + COLUMN_NAME[2]);
			else
				printWriter.println();
			// Write Table content

			if (includeTimeColumn)
				for (int row = 0; row < resultTable.getRowCount(); row++) {
					printWriter.print(resultTable.getValueAt(row, 0) + separator);
					printWriter.print(NumberFormatter.parse(resultTable.getValueAt(row, 1).toString()) + separator);
					printWriter.println(resultTable.getValueAt(row, 2));
				}
			else
				for (int row = 0; row < resultTable.getRowCount(); row++) {
					printWriter.print(resultTable.getValueAt(row, 0) + separator);
					printWriter.println(NumberFormatter.parse(resultTable.getValueAt(row, 1).toString()));
				}
			printWriter.close();
		} catch (FileNotFoundException | UnsupportedEncodingException | ParseException exception) {
			if (printWriter != null)
				printWriter.close();
		}
	}

	@Override
	public void notifyStart(int currentSetLength, long numberOfTaskToPerform) {}

	@Override
	public void notifyProgress(long numberOfTaskPerformed) {}

	@Override
	public void notifyResult(int currentSetLength, long numberOfSets, long timeInSecond, String timeFormatted) {
		addRow(currentSetLength, NumberFormatter.format(numberOfSets), timeFormatted);
	}

	@Override
	public void notifyEnd() {}
}
