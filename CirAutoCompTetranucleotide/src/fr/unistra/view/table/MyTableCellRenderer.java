package fr.unistra.view.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 * Met en forme (couleur, police) l'affichage dans une table
 * 
 * @author Abdoul-djawadou SALAOU
 */
public class MyTableCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
	private static final long	serialVersionUID	= 1L;
	private boolean	          useBlueColor	     = true;

	public MyTableCellRenderer() {
		setHorizontalAlignment(SwingConstants.CENTER);
	}

	public MyTableCellRenderer(boolean useBlueColor) {
		this.useBlueColor = useBlueColor;
		setHorizontalAlignment(SwingConstants.CENTER);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		setFont(new Font("Serif", Font.BOLD, 15));

		if (isSelected) {
			setForeground(Color.blue);
			setBackground(new Color(245, 193, 212));
		}
		else {
			setForeground(Color.black);
			if (useBlueColor)
				setBackground(row % 2 == 0 ? new Color(193, 239, 245) : Color.WHITE);
			else
				setBackground(row % 2 == 0 ? new Color(245, 193, 212) : Color.WHITE);

			setForeground(Color.black);
		}
		return this;
	}

	public void setUseBlueColor(boolean useBlueColor) {
		this.useBlueColor = useBlueColor;
	}

	public static void setHederFont(JTable table) {
		table.getTableHeader().setFont(new Font("SansSerif", Font.ITALIC | Font.BOLD, 16));
		table.getTableHeader().setForeground(Color.blue);
		((DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
	}

}
