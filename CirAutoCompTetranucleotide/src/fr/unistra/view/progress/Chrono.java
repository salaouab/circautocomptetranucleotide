package fr.unistra.view.progress;

import static fr.unistra.view.utils.viewUtils.CYAN_COLOR;
import static fr.unistra.view.utils.viewUtils.componentFont15;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

/**
 * Runs a chronometer inside a label. It uses a {@link Timer} to simulate seconds elapsing
 * 
 * @author Abdoul-djawadou SALAOU
 */
public class Chrono extends JLabel {
	private static final long	serialVersionUID	= 1L;
	private int	              day	             = 0, hour = 0, minute = 0, second = 0;
	private final static int	MILLISECOND	     = 1000;
	private Timer	          timer;
	private String	          dayString	         = "d";

	public Chrono() {
		setOpaque(false);
		setFont(componentFont15);
		setAlignmentX(Component.CENTER_ALIGNMENT);
		setHorizontalAlignment(SwingConstants.CENTER);
		setForeground(CYAN_COLOR);
		timer = new Timer(MILLISECOND, new TimerAction());
		timer.setCoalesce(false);
		setText("00 00 00s");
	}

	public void start() {
		timer.start();
	}

	public void stop() {
		timer.stop();
	}

	public void reset() {
		day = 0;
		hour = 0;
		minute = 0;
		second = 0;
		setText("");
	}

	class TimerAction implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e1) {
			second++;
			if (second == 60) {
				second = 0;
				minute++;
			}
			if (minute == 60) {
				minute = 0;
				hour++;
			}
			if (hour == 24) {
				hour = 0;
				day++;
			}
			setText(getTimeFormated());
		}

		private String getTimeFormated() {
			String timeString = "";
			if (day != 0) {
				timeString += day + dayString + " ";
				timeString += hour == 0 ? "00 " : "";
				timeString += hour == 0 && minute == 0 ? "00 " : "";
			}
			if (hour != 0) {
				timeString += hour + " ";
				timeString += minute == 0 ? "00 " : "";
			}
			if (minute != 0)
				timeString += (minute > 9 ? minute : "0" + minute) + " ";

			timeString += (second > 9 ? second : "0" + second) + "s";
			return timeString;
		}
	}

	/**
	 * @param string
	 */
	public void setDayString(String dayString) {
		this.dayString = dayString;
	}

}