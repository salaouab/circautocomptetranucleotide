package fr.unistra.view.progress;

import static fr.unistra.utils.GraphAlgorithmConstants.NumberFormatter;
import static fr.unistra.view.utils.viewUtils.*;

import java.awt.Dimension;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.unistra.utils.TaskObserver;

/**
 * Display the number of tasks to process and the current progression
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class CurrentProgressPanel extends JPanel implements TaskObserver {
	private static final long	serialVersionUID	= 1L;
	private JLabel	           tasksPerformedLabel;
	private JLabel	           setLengthLabel;
	private ProgressBarHandler	progressBarHandler	= new ProgressBarHandler();
	private JLabel	           totalTasksLabel;

	public CurrentProgressPanel() {
		createContents();
		setBackground(backgroundColor);
		LineBorder lineBorder = new LineBorder(CYAN_COLOR, 4, true);
		TitledBorder titledborder = new TitledBorder(lineBorder, "Current Progress", TitledBorder.LEADING, TitledBorder.TOP, componentFont15, foregroundColor);
		setBorder(titledborder);
		Dimension dimension = new Dimension(500, 130);
		setMinimumSize(dimension);
		setPreferredSize(new Dimension(600, 130));
		setMaximumSize(new Dimension(600, 150));
	}

	private void createContents() {
		setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		final JLabel lblSetLength = new JLabel("Set Length");
		initComponentProperties(lblSetLength);
		add(lblSetLength, "2, 2");

		final JLabel label_2 = new JLabel(" : ");
		initComponentProperties(label_2);
		add(label_2, "4, 2");

		setLengthLabel = new JLabel("4");
		initComponentProperties(setLengthLabel);
		setLengthLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(setLengthLabel, "6, 2");

		final JLabel lblTask = new JLabel("Tasks Performed");
		initComponentProperties(lblTask);
		add(lblTask, "2, 4");

		final JLabel label_3 = new JLabel(" : ");
		initComponentProperties(label_3);
		add(label_3, "4, 4");

		tasksPerformedLabel = new JLabel("203254 203");
		initComponentProperties(tasksPerformedLabel);
		totalTasksLabel = new JLabel("4 203 254  203");
		initComponentProperties(totalTasksLabel);
		JLabel label_s = new JLabel(" / ");
		initComponentProperties(label_s);
		tasksPerformedLabel.setHorizontalAlignment(SwingConstants.CENTER);
		JPanel taskPanel = new JPanel();
		taskPanel.setBackground(backgroundColor);
		taskPanel.add(tasksPerformedLabel);
		taskPanel.add(label_s);
		taskPanel.add(totalTasksLabel);
		add(taskPanel, "6, 4");

		add(Box.createHorizontalStrut(125), "2,6");
		progressBarHandler.updateProgressValue(7);
		add(progressBarHandler.getProgressBar(), "6, 6,default, fill");
	}

	@Override
	public void notifyStart(int currentSetLength, long numberOfTaskToPerform) {
		setLengthLabel.setText(currentSetLength + "");
		totalTasksLabel.setText(NumberFormatter.format(numberOfTaskToPerform));
		tasksPerformedLabel.setText("0");
		progressBarHandler.setMaxValue(numberOfTaskToPerform);
		progressBarHandler.updateProgressValue(0);
	}

	@Override
	public void notifyProgress(long numberOfTaskPerformed) {
		progressBarHandler.updateProgressValue(numberOfTaskPerformed);
		tasksPerformedLabel.setText(NumberFormatter.format(numberOfTaskPerformed));
	}

	@Override
	public void notifyResult(int currentSetLength, long numberOfSets, long timeInSecond, String timeFormatted) {
		progressBarHandler.progressComplete();
		tasksPerformedLabel.setText(totalTasksLabel.getText());
	}

	@Override
	public void notifyEnd() {}

	public static void main(String[] args) {
		// Setting look & feel for the application
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
		} catch (Exception e) {

		}
		JFrame frame = new JFrame();
		frame.getContentPane().add(new CurrentProgressPanel());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.pack();

	}
}
