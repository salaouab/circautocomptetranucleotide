package fr.unistra.view.progress;

import static fr.unistra.view.utils.viewUtils.CYAN_COLOR;
import static fr.unistra.view.utils.viewUtils.componentFont12;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;

import javax.swing.JProgressBar;
import javax.swing.Painter;
import javax.swing.UIDefaults;

/**
 * Manages progressbar updates
 * 
 * @author Abdoul-djawadou SALAOU
 */
public class ProgressBarHandler {
	private JProgressBar	progressBar;
	private int	         lastValue	   = -1;
	private int	         newValue	   = 0;
	private long	     maxInputValue	= 25;

	public ProgressBarHandler() {
		progressBar = new JProgressBar(0, 100);
		progressBar.setStringPainted(true);
		progressBar.setValue(0);
		progressBar.setFont(componentFont12);
		// progressBar.setForeground(Color.black);
		progressBar.setPreferredSize(new Dimension(300, 18));
		paintProgressBar(CYAN_COLOR);
	}

	public void paintProgressBar(Color color) {
		UIDefaults defaults = new UIDefaults();
		ProgressBarPainter barPainter = new ProgressBarPainter(color);
		defaults.put("ProgressBar[Enabled].foregroundPainter", barPainter);
		defaults.put("ProgressBar[Enabled+Finished].foregroundPainter", barPainter);
		progressBar.putClientProperty("Nimbus.Overrides.InheritDefaults", Boolean.TRUE);
		progressBar.putClientProperty("Nimbus.Overrides", defaults);
	}

	public JProgressBar getProgressBar() {
		return progressBar;
	}

	public void setProgressBar(JProgressBar progressBar) {
		this.progressBar = progressBar;
	}

	public long getMaxValue() {
		return maxInputValue;
	}

	public void setMaxValue(long maxValue) {
		maxInputValue = maxValue > 0 ? maxValue : 1;
		lastValue = -1;
	}

	public void updateProgressValue(long currentValue) {
		newValue = (int) ((100 * currentValue) / maxInputValue);
		if (newValue == lastValue)
			return;
		lastValue = newValue;
		progressBar.setValue(newValue);

	}

	public void progressComplete() {
		progressBar.setValue(100);
	}

	class ProgressBarPainter implements Painter<JProgressBar> {
		private final Color	color;

		public ProgressBarPainter(Color color) {
			this.color = color;
		}

		@Override
		public void paint(Graphics2D gd, JProgressBar t, int width, int height) {
			gd.setColor(color);
			gd.fillRect(0, 0, width, height);
		}

	}
}
