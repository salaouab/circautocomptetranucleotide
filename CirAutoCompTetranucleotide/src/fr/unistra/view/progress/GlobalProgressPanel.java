package fr.unistra.view.progress;

import static fr.unistra.view.utils.viewUtils.*;

import java.awt.Component;
import java.awt.Dimension;
import java.util.Date;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.unistra.utils.GraphAlgorithmConstants;
import fr.unistra.utils.TaskObserver;

/**
 * Displays the global progress and chrono indicating the elapsed time
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class GlobalProgressPanel extends JPanel implements TaskObserver {
	private static final long	serialVersionUID	= 1L;
	private JLabel	           setLengthLabel;
	private Chrono	           chrono	           = new Chrono();
	private ProgressBarHandler	progressBarHandler	= new ProgressBarHandler();

	public GlobalProgressPanel() {
		createContents();
		setBackground(backgroundColor);
		LineBorder lineBorder = new LineBorder(CYAN_COLOR, 4, true);
		TitledBorder titledborder = new TitledBorder(lineBorder, "Global Progress", TitledBorder.LEADING, TitledBorder.TOP, componentFont15, foregroundColor);
		setBorder(titledborder);
		Dimension dimension = new Dimension(500, 160);
		setMinimumSize(dimension);
		setPreferredSize(new Dimension(600, 155));
		setMaximumSize(new Dimension(600, 180));
	}

	private void createContents() {
		setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		final JLabel lblSetLength = new JLabel("Set Length");
		initComponentProperties(lblSetLength);
		add(lblSetLength, "2, 2");

		final JLabel label_2 = new JLabel(" : ");
		initComponentProperties(label_2);
		add(label_2, "4, 2");

		setLengthLabel = new JLabel("2");
		initComponentProperties(setLengthLabel);
		setLengthLabel.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel totalSetLabel = new JLabel(" /  " + GraphAlgorithmConstants.MAX_SET_LENGHT);
		initComponentProperties(totalSetLabel);
		JPanel setLengthPanel = new JPanel();
		setLengthPanel.add(setLengthLabel);
		setLengthPanel.add(totalSetLabel);
		setLengthPanel.setBackground(backgroundColor);
		add(setLengthPanel, "6, 2");

		final JLabel lblElapsedTime = new JLabel("Elapsed Time");
		initComponentProperties(lblElapsedTime);
		add(lblElapsedTime, "2, 4");

		final JLabel label_3 = new JLabel(" : ");
		initComponentProperties(label_3);
		add(label_3, "4, 4");
		add(chrono, "6, 4");

		final JLabel lblTask = new JLabel("Start Time");
		initComponentProperties(lblTask);
		add(lblTask, "2, 6");

		final JLabel label_4 = new JLabel(" : ");
		initComponentProperties(label_4);
		add(label_4, "4, 6");

		JLabel elapsedTime = new JLabel(new Date().toString());
		initComponentProperties(elapsedTime);
		elapsedTime.setHorizontalAlignment(SwingConstants.CENTER);
		elapsedTime.setAlignmentX(Component.CENTER_ALIGNMENT);
		add(elapsedTime, "6, 6");

		add(Box.createHorizontalStrut(125), "2,8");
		add(progressBarHandler.getProgressBar(), "6, 8,default, fill");
		progressBarHandler.setMaxValue(GraphAlgorithmConstants.MAX_SET_LENGHT);
		chrono.start();
	}

	@Override
	public void notifyStart(int currentSetLength, long numberOfTaskToPerform) {
		setLengthLabel.setText(currentSetLength + "");
		progressBarHandler.updateProgressValue(currentSetLength - 1);
	}

	@Override
	public void notifyProgress(long numberOfTaskPerformed) {}

	@Override
	public void notifyResult(int currentSetLength, long numberOfSets, long timeInSecond, String timeFormatted) {}

	@Override
	public void notifyEnd() {
		chrono.stop();
		progressBarHandler.progressComplete();
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new GlobalProgressPanel());
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
	}

}
