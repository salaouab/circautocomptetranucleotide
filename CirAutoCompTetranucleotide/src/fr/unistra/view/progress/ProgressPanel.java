package fr.unistra.view.progress;

import static fr.unistra.view.utils.viewUtils.backgroundColor;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;

/**
 * Panel displaying the global and current execution progress
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class ProgressPanel extends JPanel {
	private static final long	 serialVersionUID	= 1L;
	private CurrentProgressPanel	currentProgressPanel;
	private GlobalProgressPanel	 globalProgressPanel;

	public ProgressPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBackground(backgroundColor);
		// setPreferredSize(new Dimension(600, 130));
		setMaximumSize(new Dimension(600, 330));

		globalProgressPanel = new GlobalProgressPanel();
		currentProgressPanel = new CurrentProgressPanel();
		JSeparator separator = new JSeparator();
		separator.setForeground(Color.WHITE);

		add(Box.createVerticalGlue());
		add(currentProgressPanel);
		add(Box.createVerticalStrut(15));
		add(Box.createVerticalGlue());
		// add(separator);
		add(Box.createVerticalStrut(10));
		add(globalProgressPanel);
		add(Box.createVerticalGlue());
	}

	public CurrentProgressPanel getCurrentProgressPanel() {
		return currentProgressPanel;
	}

	public void setCurrentProgressPanel(CurrentProgressPanel currentProgressPanel) {
		this.currentProgressPanel = currentProgressPanel;
	}

	public GlobalProgressPanel getGlobalProgressPanel() {
		return globalProgressPanel;
	}

	public void setGlobalProgressPanel(GlobalProgressPanel globalProgressPanel) {
		this.globalProgressPanel = globalProgressPanel;
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new ProgressPanel());
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
	}
}
