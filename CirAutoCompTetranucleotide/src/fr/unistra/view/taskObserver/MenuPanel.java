package fr.unistra.view.taskObserver;

import static fr.unistra.utils.GraphAlgorithmConstants.readAllContent;
import static fr.unistra.view.utils.viewUtils.CYAN_COLOR;
import static fr.unistra.view.utils.viewUtils.backgroundColor;
import static fr.unistra.view.utils.viewUtils.componentFont15;
import static fr.unistra.view.utils.viewUtils.foregroundColor;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

import fr.unistra.view.chart.ChartPlotHandler;
import fr.unistra.view.utils.InformationDialog;

/**
 * Panel offering differents menu option buttons
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class MenuPanel extends JPanel {
	private static final long	serialVersionUID	= 1L;
	private JButton	          lineChartButton, barChartButton;
	private JButton	          labelButton, xLineButton, yLineButton;
	private JButton	          exportButton, aboutButton, exitButton;
	private boolean	          showXline	         = false;
	private boolean	          showYline	         = false;
	private boolean	          showLabel	         = false;
	protected ExportDialog	  exportDialog;

	public MenuPanel(final ChartPlotHandler setsChartHandler, final ChartPlotHandler timeChartHandler, final ExportDialog exportDialog) {

		lineChartButton = new JButton("Line Chart");
		lineChartButton.setFont(componentFont15);
		lineChartButton.setIcon(new ImageIcon(MenuPanel.class.getResource("/image/lineChart.png")));

		barChartButton = new JButton("Bar Chart");
		barChartButton.setFont(componentFont15);
		barChartButton.setIcon(new ImageIcon(MenuPanel.class.getResource("/image/barChart.png")));

		labelButton = new JButton("Show Labels");
		labelButton.setFont(componentFont15);
		labelButton.setIcon(new ImageIcon(MenuPanel.class.getResource("/image/label.png")));

		xLineButton = new JButton("Show X Lines");
		xLineButton.setFont(componentFont15);
		xLineButton.setIcon(new ImageIcon(MenuPanel.class.getResource("/image/x_line.png")));

		yLineButton = new JButton("Show Y Lines");
		yLineButton.setFont(componentFont15);
		yLineButton.setIcon(new ImageIcon(MenuPanel.class.getResource("/image/y_line.png")));

		exportButton = new JButton("    Export    ");
		exportButton.setFont(componentFont15);
		exportButton.setIcon(new ImageIcon(MenuPanel.class.getResource("/image/export.png")));

		aboutButton = new JButton("    About     ");
		aboutButton.setFont(componentFont15);
		aboutButton.setIcon(new ImageIcon(MenuPanel.class.getResource("/image/about.png")));

		exitButton = new JButton("    Exit      ");
		exitButton.setFont(componentFont15);
		exitButton.setIcon(new ImageIcon(MenuPanel.class.getResource("/image/exit.png")));

		JPanel chartOptionPanel = new JPanel();
		chartOptionPanel.add(lineChartButton);
		chartOptionPanel.add(barChartButton);
		chartOptionPanel.setBackground(backgroundColor);

		JPanel showHidePanel = new JPanel();
		showHidePanel.add(labelButton);
		showHidePanel.add(xLineButton);
		showHidePanel.add(yLineButton);
		showHidePanel.setBackground(backgroundColor);

		JPanel servicePanel = new JPanel();
		servicePanel.add(exportButton);
		servicePanel.add(aboutButton);
		servicePanel.add(exitButton);
		servicePanel.setBackground(backgroundColor);

		Dimension separatorDim = new Dimension(5, showHidePanel.getPreferredSize().height - 8);
		JPanel separator = new JPanel();
		separator.setPreferredSize(separatorDim);
		separator.setBackground(foregroundColor);

		JPanel separator2 = new JPanel();
		separator2.setPreferredSize(separatorDim);
		separator2.setBackground(foregroundColor);

		int structWidth = 10;

		setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
		add(showHidePanel);
		add(Box.createHorizontalStrut(structWidth));
		add(separator);
		add(Box.createHorizontalStrut(structWidth));
		add(chartOptionPanel);
		add(Box.createHorizontalStrut(structWidth));
		add(separator2);
		add(Box.createHorizontalStrut(structWidth));
		add(servicePanel);
		add(Box.createHorizontalStrut(structWidth));

		setBorder(new MatteBorder(0, 0, 5, 0, CYAN_COLOR));
		setBackground(backgroundColor);

		lineChartButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setsChartHandler.switchLineChart();
				timeChartHandler.switchLineChart();
			}
		});
		barChartButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setsChartHandler.switchBarChart();
				timeChartHandler.switchBarChart();
			}
		});
		labelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showLabel = !showLabel;
				if (showLabel) {
					labelButton.setText("Hide Labels");
					setsChartHandler.showItemLabel(true);
					timeChartHandler.showItemLabel(true);
				}
				else {
					labelButton.setText("Show  Labels");
					setsChartHandler.showItemLabel(false);
					timeChartHandler.showItemLabel(false);
				}
			}
		});
		xLineButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showXline = !showXline;
				if (showXline) {
					xLineButton.setText("Hide X Lines");
					setsChartHandler.showXLine(true);
					timeChartHandler.showXLine(true);
				}
				else {
					xLineButton.setText("Show  X Lines");
					setsChartHandler.showXLine(false);
					timeChartHandler.showXLine(false);
				}
			}
		});
		yLineButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showYline = !showYline;
				if (showYline) {
					yLineButton.setText("Hide Y Lines");
					setsChartHandler.showYLine(true);
					timeChartHandler.showYLine(true);
				}
				else {
					yLineButton.setText("Show  Y Lines");
					setsChartHandler.showYLine(false);
					timeChartHandler.showYLine(false);
				}
			}
		});
		exportButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				exportDialog.setVisible(true);
			}
		});
		
		aboutButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				InputStream fileInputStream = getClass().getResourceAsStream("/help/about_en.html");
				new InformationDialog(readAllContent(fileInputStream), new Dimension(780, 410), exportDialog.getParent()).setVisible(true);
			}
		});
		
	}

	public void setExitButtonAction(ActionListener exitAction) {
		exitButton.addActionListener(exitAction);
	}
	
}
