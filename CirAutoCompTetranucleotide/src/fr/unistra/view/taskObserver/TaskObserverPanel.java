package fr.unistra.view.taskObserver;

import static fr.unistra.view.utils.viewUtils.backgroundColor;
import static fr.unistra.view.utils.viewUtils.foregroundColor;

import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import fr.unistra.utils.TaskObserver;
import fr.unistra.view.progress.ProgressPanel;
import fr.unistra.view.table.ResultTable;

/**
 * Panel displaying the task progression and its result
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class TaskObserverPanel extends JPanel implements TaskObserver {
	private static final long	serialVersionUID	= 1L;
	private ResultTable	      resultTable;
	private ProgressPanel	  progressPanel;

	public TaskObserverPanel() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		progressPanel = new ProgressPanel();
		resultTable = new ResultTable();
		JSeparator separator = new JSeparator(SwingConstants.VERTICAL);
		separator.setForeground(foregroundColor);

		add(Box.createHorizontalGlue());
		add(progressPanel);
		add(Box.createHorizontalStrut(15));
		add(Box.createHorizontalGlue());
		// add(separator);
		add(Box.createHorizontalStrut(15));
		add(resultTable);
		add(Box.createHorizontalGlue());
		setBackground(backgroundColor);
		setMaximumSize(new Dimension(Integer.MAX_VALUE, 315));

	}

	public ResultTable getResultTable() {
		return resultTable;
	}

	@Override
	public void notifyStart(int currentSetLength, long numberOfTaskToPerform) {
		progressPanel.getCurrentProgressPanel().notifyStart(currentSetLength, numberOfTaskToPerform);
		progressPanel.getGlobalProgressPanel().notifyStart(currentSetLength, numberOfTaskToPerform);
	}

	@Override
	public void notifyProgress(long numberOfTaskPerformed) {
		progressPanel.getCurrentProgressPanel().notifyProgress(numberOfTaskPerformed);
	}

	@Override
	public void notifyResult(int currentSetLength, long numberOfSets, long timeInSecond, String timeFormatted) {
		progressPanel.getCurrentProgressPanel().notifyResult(currentSetLength, numberOfSets, timeInSecond, timeFormatted);
		resultTable.notifyResult(currentSetLength, numberOfSets, timeInSecond, timeFormatted);
	}

	@Override
	public void notifyEnd() {
		progressPanel.getGlobalProgressPanel().notifyEnd();
	}
}
