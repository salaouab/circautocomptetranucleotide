package fr.unistra.view.taskObserver;

import static fr.unistra.utils.GraphAlgorithmConstants.APP_DATA_DIR;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.*;

import fr.unistra.view.chart.ChartPlotHandler;
import fr.unistra.view.table.ResultTable;
import fr.unistra.view.utils.GeneralDialog;
import fr.unistra.view.utils.viewUtils;

/**
 * Dialog allowing to save result table and charts
 * @author Abdoul-Djawadou SALAOU
 */
public class ExportDialog extends JDialog {
	private static final long	 serialVersionUID	= 1L;
	private JRadioButton	     jpegCsvRButton;
	private JRadioButton	     pngTxtRButton;
	private String	             fileBaseName	  = new File(APP_DATA_DIR + "/result_chart").getAbsolutePath();
	private File	             lastDirFile	  = new File(APP_DATA_DIR);
	private JTextField	         pathTextField;

	public final static String	 JPEG	          = " JPEG";
	public final static String	 PNG	          = " PNG";
	// comma separated value
	public final static String	 CSV	          = " CSV";
	// tabulation separated value
	public final static String	 TXT	          = " TXT";
	public final static String[]	ExportType	  = { "Result Chart", "Time Chart", "Result Table" };

	public ExportDialog(JFrame parent, final ChartPlotHandler resultChart, final ChartPlotHandler timeChart, final ResultTable resultTable) {
		super(parent);
		JPanel panel = new JPanel();
		panel.setBackground(viewUtils.backgroundColor);
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), },
				new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblExport = new JLabel("Export ");
		viewUtils.initComponentProperties2(lblExport);
		panel.add(lblExport, "2, 2");

		JLabel label = new JLabel(":");
		viewUtils.initComponentProperties2(label);
		panel.add(label, "4, 2");
		LineBorder lineBorder1 = new LineBorder(viewUtils.CYAN_COLOR, 2, false);

		final JComboBox<String> exportTypecomboBox = new JComboBox<String>();
		exportTypecomboBox.setModel(new DefaultComboBoxModel<String>(ExportType));
		((JLabel) exportTypecomboBox.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
		exportTypecomboBox.setFont(viewUtils.componentFont15);
		exportTypecomboBox.setForeground(Color.blue);
		exportTypecomboBox.setBorder(lineBorder1);
		panel.add(exportTypecomboBox, "6, 2");

		JLabel lblFormat = new JLabel("Format");
		viewUtils.initComponentProperties2(lblFormat);
		panel.add(lblFormat, "2, 4");
		JLabel label_1 = new JLabel(":");
		viewUtils.initComponentProperties2(label_1);
		viewUtils.initComponentProperties2(label_1);
		panel.add(label_1, "4, 4");
		JPanel formatPanel = new JPanel();
		panel.add(formatPanel, "6, 4");
		formatPanel.setLayout(new BoxLayout(formatPanel, BoxLayout.X_AXIS));
		jpegCsvRButton = new JRadioButton(JPEG, true);
		viewUtils.initComponentProperties2(jpegCsvRButton);
		formatPanel.add(jpegCsvRButton);
		formatPanel.add(Box.createHorizontalStrut(40));
		pngTxtRButton = new JRadioButton(PNG, false);
		viewUtils.initComponentProperties2(pngTxtRButton);
		formatPanel.add(pngTxtRButton);
		formatPanel.setBackground(viewUtils.backgroundColor);

		ButtonGroup rbuttonGroup = new ButtonGroup();
		rbuttonGroup.add(jpegCsvRButton);
		rbuttonGroup.add(pngTxtRButton);

		final JCheckBox timeColummnCheckBox = new JCheckBox("Include Time Column", true);
		timeColummnCheckBox.setBorder(lineBorder1);
		viewUtils.initComponentProperties2(timeColummnCheckBox);
		timeColummnCheckBox.setVisible(false);

		final JSpinner widthSpinner = new JSpinner();
		widthSpinner.setModel(new SpinnerNumberModel(600, 100, 100000, 10));
		widthSpinner.setBorder(lineBorder1);
		centerSpinnerText(widthSpinner);
		final JSpinner hightSpinner = new JSpinner();
		hightSpinner.setModel(new SpinnerNumberModel(400, 50, 100000, 10));
		hightSpinner.setBorder(lineBorder1);
		centerSpinnerText(hightSpinner);

		final JPanel sizePanel = new JPanel();
		sizePanel.setLayout(new BoxLayout(sizePanel, BoxLayout.X_AXIS));
		JLabel widthLabel = new JLabel("Width ");
		viewUtils.initComponentProperties2(widthLabel);
		sizePanel.add(widthLabel);
		sizePanel.add(widthSpinner);
		sizePanel.add(Box.createHorizontalStrut(20));
		JLabel hightLabel = new JLabel("Hight ");
		viewUtils.initComponentProperties2(hightLabel);
		sizePanel.add(hightLabel);
		sizePanel.add(hightSpinner);
		sizePanel.setBackground(viewUtils.backgroundColor);

		final JLabel sizeLabel = new JLabel("Image Size ");
		viewUtils.initComponentProperties2(sizeLabel);
		final JLabel sizePointLabel = new JLabel(":");
		viewUtils.initComponentProperties2(sizePointLabel);

		JPanel optionPanel = new JPanel();
		optionPanel.setLayout(new BoxLayout(optionPanel, BoxLayout.X_AXIS));
		optionPanel.add(timeColummnCheckBox);
		optionPanel.add(sizePanel);
		optionPanel.setBackground(viewUtils.backgroundColor);

		panel.add(sizeLabel, "2, 6");
		panel.add(sizePointLabel, "4, 6");
		panel.add(optionPanel, "6, 6");

		JLabel lblPath = new JLabel("Path To Save Data");
		viewUtils.initComponentProperties2(lblPath);
		panel.add(lblPath, "2, 8");
		JLabel label_2 = new JLabel(":");
		viewUtils.initComponentProperties2(label_2);
		panel.add(label_2, "4, 8");
		JPanel pathPanel = new JPanel();
		panel.add(pathPanel, "6, 8, fill, fill");
		pathPanel.setLayout(new BoxLayout(pathPanel, BoxLayout.X_AXIS));
		pathPanel.setBackground(viewUtils.backgroundColor);

		pathTextField = new JTextField();
		LineBorder lineBorder = new LineBorder(viewUtils.CYAN_COLOR, 3, false);
		pathTextField.setBorder(lineBorder);
		pathTextField.setMargin(new Insets(3, 20, 3, 20));
		pathTextField.setDragEnabled(true);
		pathTextField.setFont(viewUtils.componentFont15);
		setPathDir(fileBaseName + ".jpeg");

		pathPanel.add(pathTextField);
		JButton browserButton = new JButton("  Browser  ");
		pathPanel.add(browserButton);
		optionPanel.add(Box.createHorizontalStrut(browserButton.getPreferredSize().width));

		JSeparator separator_1 = new JSeparator();
		panel.add(separator_1, "2, 10");
		JSeparator separator = new JSeparator();
		panel.add(separator, "6, 10");

		JPanel buttonPanel = new JPanel();
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		JButton exportButton = new JButton("   Export   ");
		exportButton.setFont(viewUtils.componentFont15);
		buttonPanel.add(exportButton);
		Component horizontalStrut = Box.createHorizontalStrut(20);
		buttonPanel.add(horizontalStrut);
		JButton cancelButton = new JButton("  Cancel  ");
		cancelButton.setFont(viewUtils.componentFont15);
		buttonPanel.add(cancelButton);
		buttonPanel.setBackground(viewUtils.backgroundColor);

		setMinimumSize(new Dimension(700, 250));
		setLocationRelativeTo(parent);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		setModal(true);
		setTitle("Save data");
		setVisible(false);
		exportTypecomboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				switch (exportTypecomboBox.getSelectedItem().toString()) {
					case "Result Chart":
					case "Time Chart":
						jpegCsvRButton.setText(JPEG);
						pngTxtRButton.setText(PNG);
						timeColummnCheckBox.setVisible(false);
						sizeLabel.setVisible(true);
						sizePointLabel.setVisible(true);
						sizePanel.setVisible(true);
						break;
					case "Result Table":
						jpegCsvRButton.setText(CSV);
						pngTxtRButton.setText(TXT);
						timeColummnCheckBox.setVisible(true);
						sizeLabel.setVisible(false);
						sizePointLabel.setVisible(false);
						sizePanel.setVisible(false);
						break;
				}
				setPathDir(fileBaseName + getCurrentSeletedExtension());
			}
		});
		browserButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int result;
				JFileChooser fileChooser = new JFileChooser(lastDirFile);
				result = fileChooser.showSaveDialog(ExportDialog.this);
				if (result != JFileChooser.APPROVE_OPTION)
					return;

				fileBaseName = getFileBaseName(fileChooser.getSelectedFile().getAbsolutePath());
				setPathDir(fileBaseName + getCurrentSeletedExtension());
				lastDirFile = fileChooser.getSelectedFile().getParentFile();
			}
		});
		jpegCsvRButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setPathDir(fileBaseName + getCurrentSeletedExtension());

			}
		});
		pngTxtRButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setPathDir(fileBaseName + getCurrentSeletedExtension());

			}
		});
		exportButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				File saveFile = new File(pathTextField.getText());
				int width = (int) widthSpinner.getValue();
				int height = (int) hightSpinner.getValue();
				boolean includeTimeColumn = timeColummnCheckBox.isSelected();
				switch (exportTypecomboBox.getSelectedItem().toString()) {
					case "Result Chart":
						if (jpegCsvRButton.isSelected())
							resultChart.saveAsJPEG(saveFile, width, height);
						else
							resultChart.saveAsPNG(saveFile, width, height);
						break;
					case "Time Chart":
						if (jpegCsvRButton.isSelected())
							timeChart.saveAsJPEG(saveFile, width, height);
						else
							timeChart.saveAsPNG(saveFile, width, height);
						break;
					case "Result Table":
						if (jpegCsvRButton.isSelected())
							resultTable.saveTableToFile(saveFile, ",", includeTimeColumn);
						else
							resultTable.saveTableToFile(saveFile, "\t", includeTimeColumn);
						break;
				}
				// JOptionPane.showMessageDialog(ExportDialog.this,
				// "File successfully saved to the disk:  " + saveFile.getAbsolutePath(),
				// "Information", JOptionPane.INFORMATION_MESSAGE);
				GeneralDialog.showInformation("File successfully saved to the disk:  " + saveFile.getAbsolutePath(), "Confirmation",
				                              ExportDialog.this);
				setVisible(false);
			}
		});
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
	}

	public void setPathDir(String filePath) {
		pathTextField.setText(filePath);
		pathTextField.setCaretPosition(filePath.length());
	}

	public String getFileBaseName(String filePath) {
		String[] tokens = filePath.split("\\.(?=[^\\.]+$)");
		return tokens[0];
	}

	public String getCurrentSeletedExtension() {
		String extension = jpegCsvRButton.isSelected() ? jpegCsvRButton.getText() : pngTxtRButton.getText();
		return "." + extension.trim().toLowerCase();
	}

	public static void centerSpinnerText(JSpinner spinner) {
		JSpinner.DefaultEditor spinnerEditor = (JSpinner.DefaultEditor) spinner.getEditor();
		spinnerEditor.getTextField().setHorizontalAlignment(SwingConstants.CENTER);
		spinner.setFont(viewUtils.componentFont12);
	}

	public static void main(String[] args) {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
		} catch (Exception e) {}

		ExportDialog d = new ExportDialog(null, null, null, null);
		d.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		d.setLocationRelativeTo(null);
		d.setVisible(true);
	}
}
