package fr.unistra.view.taskObserver;

import static fr.unistra.utils.GraphAlgorithmConstants.INTERUPTOR_FLAG;
import static fr.unistra.utils.GraphAlgorithmConstants.deleteTmpDir;
import static fr.unistra.view.utils.viewUtils.backgroundColor;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import fr.unistra.utils.TaskObserver;
import fr.unistra.view.chart.SetsChartPanel;
import fr.unistra.view.chart.TimeChartPanel;
import fr.unistra.view.utils.GeneralDialog;

/**
 * The main frame of the application
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class TaskFrame extends JFrame implements TaskObserver {
	private static final long	serialVersionUID	= 1L;
	private MenuPanel	      menuPanel;
	private SetsChartPanel	  setsChartPanel;
	private TimeChartPanel	  timeChartPanel;
	private TaskObserverPanel	taskObserverPanel;
	private ExportDialog	  exportDialog;
	private boolean	          taskComplete	     = false;

	public TaskFrame() {
		setsChartPanel = new SetsChartPanel();
		timeChartPanel = new TimeChartPanel();
		taskObserverPanel = new TaskObserverPanel();
		exportDialog = new ExportDialog(this, setsChartPanel.getSetsChartPlot(), timeChartPanel.getTimeChartPlot(), taskObserverPanel.getResultTable());
		menuPanel = new MenuPanel(setsChartPanel.getSetsChartPlot(), timeChartPanel.getTimeChartPlot(), exportDialog);
		menuPanel.setExitButtonAction(new WindowCloseActionListener());

		JSplitPane chartsplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, setsChartPanel, timeChartPanel);
		chartsplitPane.setOneTouchExpandable(true);
		chartsplitPane.setResizeWeight(0.5);
		chartsplitPane.setBackground(backgroundColor);

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, chartsplitPane, taskObserverPanel);
		splitPane.setOneTouchExpandable(true);
		splitPane.setResizeWeight(1);
		splitPane.setBackground(backgroundColor);

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(menuPanel, BorderLayout.NORTH);
		getContentPane().add(splitPane, BorderLayout.CENTER);
		// setSize(new Dimension(1250, 700));
		setBackground(backgroundColor);
		setTitle("CircularTetranuCode");
		try {
			URL iconURL = getClass().getResource("/image/circularTetranuCode.png");
			ImageIcon icon = new ImageIcon(iconURL);
			setIconImage(icon.getImage());
		} catch (Exception e) {}
		pack();
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setLocationRelativeTo(null);
		addWindowListener(new WindowCloseAction());
		

	}

	@Override
	public void notifyStart(int currentSetLength, long numberOfTaskToPerform) {
		taskObserverPanel.notifyStart(currentSetLength, numberOfTaskToPerform);
	}

	@Override
	public void notifyProgress(long numberOfTaskPerformed) {
		taskObserverPanel.notifyProgress(numberOfTaskPerformed);
	}

	@Override
	public void notifyResult(int currentSetLength, long numberOfSets, long timeInSecond, String timeFormatted) {
		taskObserverPanel.notifyResult(currentSetLength, numberOfSets, timeInSecond, timeFormatted);
		setsChartPanel.notifyResult(currentSetLength, numberOfSets, timeInSecond, timeFormatted);
		timeChartPanel.notifyResult(currentSetLength, numberOfSets, timeInSecond, timeFormatted);
	}

	@Override
	public void notifyEnd() {
		taskObserverPanel.notifyEnd();
		taskComplete = true;
	}

	public void onExit() {
		if (taskComplete)
			System.exit(0);

		boolean choice = GeneralDialog.showQuestion("Task in progress. Do you really want to interrupt and exit?", "Exit Confirmation", this);
		if (!choice)
			return;

		INTERUPTOR_FLAG = true;
		deleteTmpDir();
		System.exit(0);
	}

	/**
	 * Handles main window closing
	 */
	class WindowCloseAction extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			onExit();
		}
	}

	class WindowCloseActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			onExit();
		}
	}

	public static void main(String[] args) {
		// Setting look & feel for the application
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
		} catch (Exception e) {}
		new TaskFrame().setVisible(true);
	}
}
