package fr.unistra.launcher;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jgrapht.experimental.dag.DirectedAcyclicGraph.CycleFoundException;

import fr.unistra.controller.TaskExecutor;
import fr.unistra.controller.TaskManager;

/**
 * Console launcher for the exploration and discovery of the circular auto complementary set
 * @author Abdoul-Djawadou SALAOU
 */
public class ConsoleLauncher {

	public static void main(String[] args) throws CycleFoundException, IOException {
		// Tasks manager
		TaskManager taskManager = new TaskManager();
		// Number of processors
		int nbProcess = Runtime.getRuntime().availableProcessors();
		// Thread pool with fixed number of thread
		ExecutorService taskExecutorPool = Executors.newFixedThreadPool(nbProcess);
		// instance the tasks executors
		for (int i = 0; i < nbProcess; i++)
			taskExecutorPool.execute(new TaskExecutor(taskManager));

		// Usefull for the threads to exit when done
		taskExecutorPool.shutdown();
	}
}
