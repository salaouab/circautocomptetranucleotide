package fr.unistra.launcher;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import fr.unistra.controller.TaskExecutor;
import fr.unistra.controller.TaskManager;
import fr.unistra.view.taskObserver.TaskFrame;

public class GuiLauncher {

	/**
	 * Graphic launcher for the exploration and discovery of the circular tetranucleotide auto
	 * complementary set. There is no need for arguments
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// Setting look & feel for the application
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
		} catch (Exception e) {}

		TaskFrame taskFrame = new TaskFrame();
		taskFrame.setVisible(true);
		// Tasks manager
		TaskManager taskManager = new TaskManager(taskFrame);
		// Number of processors
		int nbProcess = Runtime.getRuntime().availableProcessors();
		// Thread pool with fixed number of thread
		ExecutorService taskExecutorPool = Executors.newFixedThreadPool(nbProcess);
		// instance the tasks executors
		for (int i = 0; i < nbProcess; i++)
			taskExecutorPool.execute(new TaskExecutor(taskManager));

		// Usefull for the threads to exit when done
		taskExecutorPool.shutdown();
	}
}
