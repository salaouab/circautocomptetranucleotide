package fr.unistra.utils;

public class Timer {
	public long	start;
	public long	stop;
	/** time in second */
	public long	lastElapsedTime	= 0;
	public long	day	            = 0, hour = 0, minute = 0, second;

	public void start() {
		start = System.currentTimeMillis();
	}

	public void stop() {
		stop = System.currentTimeMillis();
		lastElapsedTime = (stop - start) / 1000;
		second = lastElapsedTime;
		if (second >= 60) {
			minute = second / 60;
			second = second % 60;
		}
		if (minute >= 60) {
			hour = minute / 60;
			minute = minute % 60;
		}
		if (hour >= 24) {
			day = hour / 24;
			hour = hour % 24;
		}
	}

	public String elapsed() {
		String timeFormated = "";
		if (day > 0)
			timeFormated += day + "d ";
		if (hour > 0)
			timeFormated += hour + "h ";
		if (minute > 0)
			timeFormated += minute + "m ";
		// if (second > 0)
		timeFormated += second + "s";

		return timeFormated;
	}
}
