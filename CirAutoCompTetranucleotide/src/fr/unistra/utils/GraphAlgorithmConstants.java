package fr.unistra.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import fr.unistra.model.StringEdge;
import fr.unistra.model.Tetranucleotide;

/**
 * Holds the Algorith main constants (attributes, methods) like the exploration sets,
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class GraphAlgorithmConstants {
	// Maximum set lenght
	public final static int	                       MAX_SET_LENGHT	      = 60;
	public final static int	                       MAX_SET_PER_FILE	      = 100000;
	// Maximum self complementary element in set
	public static final int	                       MAX_SELF_COMPLEMENTARY	= 6;
	// Where data are store
	public static final String	                   APP_DATA_DIR	          = "Application_data";
	public static final String	                   SAVE_DIR	              = APP_DATA_DIR + "/temp";
	public static final String	                   SAVE_DIR2	          = APP_DATA_DIR + "/temp";
	public static final String	                   FILE_PREF_NAME	      = "tetraSet";
	// temporary file name convention
	public static final String	                   FILE_NAME	          = SAVE_DIR + "/" + FILE_PREF_NAME;
	public static final String	                   FILE_NAME_S114	      = SAVE_DIR2 + "/" + FILE_PREF_NAME + "_s";

	// For the sake of debugging
	public static final boolean	                   DEBUG_MODE	          = false;
	public static final boolean	                   SAVE_RESULT_IN_FILE	  = true;
	public static boolean	                       INTERUPTOR_FLAG	      = false;
	// false means sets (S_12, S_114, ...) are not initialized
	public static boolean	                       areListInitialized	  = false;
	// set of the 114 tetranucleotides
	public final static ArrayList<Tetranucleotide>	S_114	              = new ArrayList<Tetranucleotide>();
	// set of the complementary of the 114 tetranucleotides
	public final static ArrayList<Tetranucleotide>	CS_114	              = new ArrayList<Tetranucleotide>();
	// 12 auto complementary
	public final static ArrayList<Tetranucleotide>	S_12	              = new ArrayList<Tetranucleotide>();
	// 16 to not use because it will in any way create cycles
	public final static String[]	               S_16	                  = { "AAAA", "ACAC", "AGAG", "ATAT", "CACA", "CCCC", "CGCG", "CTCT", "GAGA",
		"GCGC", "GGGG", "GTGT", "TATA", "TCTC", "TGTG", "TTTT" };
	public final static DecimalFormat	           NumberFormatter	      = new DecimalFormat("#,###", new DecimalFormatSymbols());
	public final static String[]	               COLUMN_NAME	          = { " Set Length ", " Number Of Sets ", " Elapsed Time " };

	/**
	 * Check wether the tetranucleotide is part of S_16 or not
	 * 
	 * @param tetranucleotide Tetranucleotide to check
	 * @return Return true if the S_16 is part of S_16
	 */
	public static boolean isS_16(String tetranucleotide) {
		for (String s : S_16)
			if (tetranucleotide.equals(s))
				return true;
		return false;
	}

	/**
	 * Initialize sets
	 */
	public static void initTetranucleotideSets() {
		if (areListInitialized)
			return;

		ArrayList<String> s_12 = new ArrayList<String>();
		ArrayList<String> s_114 = new ArrayList<String>();
		ArrayList<String> cs_114 = new ArrayList<String>();
		String tetranucleotide;
		String complementaryT;
		String[] letterArray = { "A", "C", "G", "T" };
		for (String letter1 : letterArray)
			for (String letter2 : letterArray)
				for (String letter3 : letterArray)
					for (String letter4 : letterArray) {
						tetranucleotide = letter1 + letter2 + letter3 + letter4;
						if (isS_16(tetranucleotide))
							continue;
						complementaryT = complementary(tetranucleotide);
						if (complementaryT.equals(tetranucleotide))
							s_12.add(tetranucleotide);
						else if (s_114.contains(complementaryT))
							cs_114.add(tetranucleotide);
						else if (!arePermuted(tetranucleotide, complementaryT))
							s_114.add(tetranucleotide);
					}

		for (String tetranucleotid : s_12)
			S_12.add(new Tetranucleotide(tetranucleotid));

		for (String tetranucleotid : s_114)
			S_114.add(new Tetranucleotide(tetranucleotid));

		// Number formatter
		NumberFormatter.getDecimalFormatSymbols().setGroupingSeparator(' ');

		areListInitialized = true;
	}

	/**
	 * Computes the complementary of the tetranucleotide
	 * 
	 * @param tetranucleotide Tetranucleotide to complement
	 * @return Return the complementary of the tetranucleotide
	 */
	public static String complementary(String tetranucleotide) {
		StringBuilder complementary = new StringBuilder();
		char[] nucleotideTab = tetranucleotide.toCharArray();
		for (int i = nucleotideTab.length - 1; i >= 0; i--) {
			switch (nucleotideTab[i]) {
				case 'A':
					complementary.append('T');
					break;
				case 'C':
					complementary.append('G');
					break;
				case 'G':
					complementary.append('C');
					break;
				case 'T':
					complementary.append('A');
					break;
			}
		}
		return complementary.toString();
	}

	/**
	 * Computes the direct permuted of the tetranucleotide example : input ATCG output TCGA
	 * 
	 * @param tetranucleotide Tetranucleotide to permut
	 * @return Return the direct permuted of the tetranucleotide
	 */
	public static String permuted(String tetranucleotide) {
		return tetranucleotide.substring(1) + tetranucleotide.charAt(0);
	}

	/**
	 * Check if the tetranucleotides are mutual permuted or equal
	 * 
	 * @param tetranucleotide First tetranucleotide
	 * @param tetranucleotide1 Second tetranucleotide
	 * @return Return true if tetranucleotides are permuted, false otherwise
	 */
	public static boolean arePermuted(String tetranucleotide, String tetranucleotide1) {
		// first permutation
		String permutedT = permuted(tetranucleotide1);
		if (tetranucleotide.equals(permutedT))
			return true;

		// second permutation
		permutedT = permuted(permutedT);
		if (tetranucleotide.equals(permutedT))
			return true;

		// third permutation
		permutedT = permuted(permutedT);
		if (tetranucleotide.equals(permutedT))
			return true;
		return tetranucleotide.equals(tetranucleotide1);
	}

	/**
	 * Compute the edges generated by this tetranucleotide and add them to the list
	 * 
	 * @param tetranucleotide Tetranucleotide to performe edges generation
	 * @param edgeList List to add the edges
	 */
	public static void tetranucleotideEdge(String tetranucleotide, ArrayList<StringEdge> edgeList) {
		for (int i = 0; i < 3; i++)
			edgeList.add(new StringEdge(tetranucleotide.substring(0, i + 1), tetranucleotide.substring(i + 1)));
	}

	/**
	 * Compute the edges generated by this tetranucleotide and add them to a list
	 * 
	 * @param tetranucleotide Tetranucleotide to performe edges generation
	 * @return Return a list containing the tetranucleotide edges
	 * @see #tetranucleotideEdge(ArrayList, ArrayList)
	 */
	public static ArrayList<StringEdge> tetranucleotideEdge(String tetranucleotide) {
		ArrayList<StringEdge> edgeList = new ArrayList<StringEdge>();
		tetranucleotideEdge(tetranucleotide, edgeList);
		return edgeList;
	}

	/**
	 * Compute the edges generated by this list of tetranucleotides and add them to a list
	 * 
	 * @param tetranucleotideList Tetranulceotides list
	 * @param edgeList List to add the edges
	 * @see #tetranucleotideEdge(String, ArrayList)
	 */
	public static void tetranucleotideEdge(ArrayList<String> tetranucleotideList, ArrayList<StringEdge> edgeList) {
		for (String tetranucleotide : tetranucleotideList)
			tetranucleotideEdge(tetranucleotide, edgeList);
	}

	/**
	 * Compute the edges generated by this list of tetranucleotides and add them to a list
	 * 
	 * @param tetranucleotideList Tetranulceotides list * @return Return a list containing the
	 *            tetranucleotide edges @see #tetranucleotideEdge(String, ArrayList)
	 */
	public static ArrayList<StringEdge> tetranucleotideEdge(ArrayList<String> tetranucleotideList) {
		ArrayList<StringEdge> edgeList = new ArrayList<StringEdge>();
		tetranucleotideEdge(tetranucleotideList, edgeList);
		return edgeList;
	}

	/**
	 * Compute the edges generated by this tetranucleotide and its compelementary and add them to
	 * the list
	 * 
	 * @param tetranucleotide Tetranucleotide to performe edges generation
	 * @param edgeList List to add the edges
	 * @see #tetranucleotideEdge(String, ArrayList)
	 */

	public static void tetraAndCompEdge(String tetranucleotide, ArrayList<StringEdge> edgeList) {
		tetranucleotideEdge(tetranucleotide, edgeList);
		String complemTetra = complementary(tetranucleotide);
		if (complemTetra.equals(tetranucleotide))
			return;
		tetranucleotideEdge(complemTetra, edgeList);
	}

	/**
	 * Compute the edges generated by this tetranucleotide and its compelementary and add them to
	 * the list
	 * 
	 * @param tetranucleotide Tetranucleotide to performe edges generation
	 * @return Return a list containg the edges generated
	 * @see #tetraAndCompEdge(String, ArrayList)
	 */
	public static ArrayList<StringEdge> tetraAndCompEdge(String tetranucleotide) {
		ArrayList<StringEdge> edgeList = new ArrayList<StringEdge>();
		tetraAndCompEdge(tetranucleotide, edgeList);
		return edgeList;
	}

	/**
	 * Compute the edges generated by the l tetranucleotides list and their complementaries and add
	 * them to a list
	 * 
	 * @param tetranucleotide Tetranucleotides list to performe edges generation
	 * @return Return a list containg the edges generated
	 * @see #tetraAndCompEdge(String, ArrayList)
	 */

	public static ArrayList<StringEdge> tetraAndCompEdge(ArrayList<String> tetranucleotideList) {
		ArrayList<StringEdge> edgeList = new ArrayList<StringEdge>();
		for (String tetranucleotide : tetranucleotideList) {
			tetraAndCompEdge(tetranucleotide, edgeList);
		}
		return edgeList;
	}

	public static void deleteTmpDir() {
		deleteTmpDir(SAVE_DIR);
		deleteTmpDir(SAVE_DIR2);
	}

	public static void deleteTmpDir(String dirPath) {
		File tmpDir = new File(dirPath);
		if (!tmpDir.exists())
			return;
		try {
			File[] contentFile = tmpDir.listFiles();
			for (File file : contentFile) {
				try {
					file.delete();
				} catch (Exception exception) {}
			}
			tmpDir.delete();
		} catch (Exception exception) {}
	}
	
	public static String readAllContent(InputStream fileStream) {
		String data = "";
		try {

			InputStreamReader fileReader = new InputStreamReader(fileStream);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			while ((line = bufferedReader.readLine()) != null)
				data += line;

			bufferedReader.close();
			fileReader.close();
			fileStream.close();
		} catch (IOException e) {
		}
		return data;
	}
}
