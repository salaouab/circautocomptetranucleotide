package fr.unistra.utils;

/**
 * Interface defining task observers contracts
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public interface TaskObserver {
	/**
	 * Being notify for actions to permfor before task starting
	 * 
	 * @param currentSetLength Set length being processing
	 * @param numberOfTaskToPerform Number of task to perform for this set length
	 */
	public void notifyStart(int currentSetLength, long numberOfTaskToPerform);

	/**
	 * Being notify for task progression
	 * 
	 * @param nomberOfTaskPerformed Number of task already processed
	 */
	public void notifyProgress(long numberOfTaskPerformed);

	/**
	 * Being notify for a result
	 * 
	 * @param currentSetLength Set length performed
	 * @param numberOfSets Number of sets found for the run
	 * @param timeInSecond Elapsed time since the run start
	 * @param timeFormatted Elapsed time formatted into string
	 */
	public void notifyResult(int currentSetLength, long numberOfSets, long timeInSecond, String timeFormatted);

	/**
	 * Notify for the end of processing
	 */
	public void notifyEnd();
}
