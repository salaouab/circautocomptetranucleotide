package fr.unistra.utils;

/**
 * Interface allowing task to be observable
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public interface TaskObservable {
	/**
	 * Add a task evolution observer
	 * 
	 * @param observer Observer to watch out the task progression
	 */
	public void addTaskObserver(TaskObserver observer);

	/**
	 * Remove all the registered observers
	 */
	public void removeObservers();

}
