package fr.unistra.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Manage the circular auto complementary tetranucleotide set. To avoid data duplication, it holdes
 * the set base and the suffixe that can be append to it at this level of exploration.
 * @author Abdoul-Djawadou SALAOU
 */
public class CircularComplementatryTetranucleotide implements Serializable {
	private static final long	                 serialVersionUID	         = 1L;
	// circular auto complementary tetranucleotide set. This is the base set
	private ArrayList<Tetranucleotide>	         complementaryTetranucleotideSet;
	// Array holding the tetranucleotides that create cycle in the above set graph
	private ArrayList<Tetranucleotide>	         cyclicTetranucleotide;
	// suffixes of the tetranucleotide set
	private ArrayList<SuffixTetranucleotide>	 suffixList	                 = null;
	// Search set for the exploration; can be S_12 or S_114
	private transient ArrayList<Tetranucleotide>	searchSet;
	private transient boolean	                             isUsingAutoComplementarySet	= false;
	// number of S_12 element in this set
	private int	                                 numberOfSelfComplementary	 = 0;
	// Remember the last exploration place
	private transient int	                     startIndex	                 = 0;
	private transient int	                     indexOfS_12	             = 0;
	private transient int	                     indexOfS_114	             = 0;

	/**
	 * Create a new set based on the tetraSet data
	 * @param tetraSet the model of creation
	 * @throws CloneNotSupportedException
	 */
	@SuppressWarnings("unchecked")
	public CircularComplementatryTetranucleotide(CircularComplementatryTetranucleotide tetraSet) throws CloneNotSupportedException {

		complementaryTetranucleotideSet = (ArrayList<Tetranucleotide>) tetraSet.complementaryTetranucleotideSet.clone();
		cyclicTetranucleotide = (ArrayList<Tetranucleotide>) tetraSet.cyclicTetranucleotide.clone();

		numberOfSelfComplementary = tetraSet.numberOfSelfComplementary;
		indexOfS_12 = tetraSet.indexOfS_12;
		indexOfS_114 = tetraSet.indexOfS_114;
	}

	/**
	 * Constructor with initialisation of sets
	 */
	public CircularComplementatryTetranucleotide() {
		complementaryTetranucleotideSet = new ArrayList<Tetranucleotide>();
		cyclicTetranucleotide = new ArrayList<Tetranucleotide>();
	}

	/**
	 * Add a tetranucleotide to the complementary set
	 * @param tetranucleotide The tetranucleotide to add the list
	 */
	public void addTetranucleotide(Tetranucleotide tetranucleotide) {
		complementaryTetranucleotideSet.add(tetranucleotide);
	}

	/**
	 * Add a tetranucleotide to the cyclic list
	 * @param tetranucleotide Tetranucleotide to add to the list
	 */
	public void addCyclicTetranucleotide(Tetranucleotide tetranucleotide) {
		cyclicTetranucleotide.add(tetranucleotide);
	}

	/**
	 * Add a suffix tetranucleotide the list
	 * @param suffix The suffix tetranucleotide to add the list
	 */
	public void addSuffixTetranucleotide(SuffixTetranucleotide suffix) {
		if (suffixList == null)
			suffixList = new ArrayList<SuffixTetranucleotide>();
		suffixList.add(suffix);
	}

	/**
	 * Return the base set clone with the suffix tetranucleotide at the suffixIndex appended
	 * @param suffixIndex Index of the suffix tetranucleotide to append to the base set
	 * @return Return the base set appended with the suffix tetranucleotide at the suffixIndex
	 *         position
	 */
	public CircularComplementatryTetranucleotide getBaseSuffix(int suffixIndex) {
		CircularComplementatryTetranucleotide baseSuffix = null;
		try {
			baseSuffix = new CircularComplementatryTetranucleotide(this);
			SuffixTetranucleotide suffix = suffixList.get(suffixIndex);
			baseSuffix.addTetranucleotide(suffix.getTetranucleotide());
			baseSuffix.setIndexOfS_12(suffix.getIndexOfS_12());
			baseSuffix.setIndexOfS_114(suffix.getIndexOfS_114());
		} catch (CloneNotSupportedException exception) {
			exception.printStackTrace();
		} catch (NullPointerException exception) {}

		return baseSuffix;
	}

	public ArrayList<Tetranucleotide> getComplementaryTetranucleotideSet() {
		return complementaryTetranucleotideSet;
	}

	public void setComplementaryTetranucleotideSet(ArrayList<Tetranucleotide> complementaryTetranucleotideSet) {
		this.complementaryTetranucleotideSet = complementaryTetranucleotideSet;
	}

	public ArrayList<Tetranucleotide> getCyclicTetranucleotide() {
		return cyclicTetranucleotide;
	}

	public void setCyclicTetranucleotide(ArrayList<Tetranucleotide> cyclicTetranucleotide) {
		this.cyclicTetranucleotide = cyclicTetranucleotide;
	}

	public ArrayList<SuffixTetranucleotide> getSuffixList() {
		return suffixList;
	}

	public void setSuffixList(ArrayList<SuffixTetranucleotide> suffixList) {
		this.suffixList = suffixList;
	}

	public int getNumberOfSelfComplementary() {
		return numberOfSelfComplementary;
	}

	public void setNumberOfSelfComplementary(int numberOfSelfComplementary) {
		this.numberOfSelfComplementary = numberOfSelfComplementary;
	}

	public void incNumberOfSelfComplementary() {
		numberOfSelfComplementary++;
	}

	public int getIndexOfS_12() {
		return indexOfS_12;
	}

	public void setIndexOfS_12(int indexOfS_12) {
		this.indexOfS_12 = indexOfS_12;
	}

	public int getIndexOfS_114() {
		return indexOfS_114;
	}

	public void setIndexOfS_114(int indexOfS_114) {
		this.indexOfS_114 = indexOfS_114;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public ArrayList<Tetranucleotide> getSearchSet() {
		return searchSet;
	}

	public void setSearchSet(ArrayList<Tetranucleotide> searchSet) {
		this.searchSet = searchSet;
	}

	public boolean isUsingAutoComplementarySet() {
		return isUsingAutoComplementarySet;
	}

	public void setUsingAutoComplementarySet(boolean isUsingAutoComplementarySet) {
		this.isUsingAutoComplementarySet = isUsingAutoComplementarySet;
	}

	public boolean isSuffixListEmpty() {
		return suffixList == null || suffixList.isEmpty();
	}

	public int getNumberOfSuffixes() {
		return suffixList != null ? suffixList.size() : 0;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return new CircularComplementatryTetranucleotide(this);
	}

	public boolean isCyclicTetranucleotide(Tetranucleotide tetranucleotide) {
		return !cyclicTetranucleotide.isEmpty() && cyclicTetranucleotide.contains(tetranucleotide);
	}

}
