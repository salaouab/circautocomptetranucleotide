package fr.unistra.model;

import java.io.*;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

import net.jpountz.lz4.*;
import fr.unistra.utils.GraphAlgorithmConstants;

/**
 * Class for data sets serialization writing in the files
 * @author Abdoul-Djawadou SALAOU
 */
public class TaskObjectWriter {
	private ObjectOutputStream	 objectOutputStream;
	private BufferedOutputStream	bufferedOutputStream;
	private FileOutputStream	 fileOutputStream;
	private String	             filePath;
	private RandomAccessFile	 file;
	private boolean	             closed	           = false;
	private boolean	             deleted	       = false;

	private static final int	 BUFFER_SIZE	   = 419304;
	/** java Deflator compression: Very interresting in term of output file size but very slow */
	private DeflaterOutputStream	compressorOutput;
	private static final int	 COMPRESSION_LEVEL	= 1;	                       // 0 - 9
	private Deflater	         compressor;

	/** LZ4 compression: Very interesting on velocity */
	private LZ4Factory	         factory	       = LZ4Factory.fastestInstance();
	private LZ4Compressor	     LZ4compressor	   = factory.fastCompressor();
	private LZ4BlockOutputStream	lz4BlockOutputStream;

	/**
	 * Open the file for writing.
	 * @param filePath File path
	 * @see #open(String)
	 */
	public TaskObjectWriter(String filePath) {
		closed = true;
		open(filePath);
	}

	/**
	 * Write the object to the output stream
	 * @param object Object to write to the stream
	 */
	public void writeObject(CircularComplementatryTetranucleotide object) {
		try {
			objectOutputStream.writeObject(object);
		} catch (IOException exception) {
			if (GraphAlgorithmConstants.DEBUG_MODE)
				exception.printStackTrace();
		}
	}

	/**
	 * Flushes data to the output
	 */
	public void flush() {
		try {
			objectOutputStream.flush();
		} catch (IOException exception) {}
	}

	/**
	 * Open the file for data writing. Currently LZ4 compression is used to minimize file size on
	 * disc
	 * @param filePath File path
	 * @see #openLZ4(String)
	 * @see #openRaw(String)
	 * @see #openDeflate(String)
	 */
	public void open(String filePath) {
		openLZ4(filePath);
	}

	/**
	 * Open file for data writing using LZ4 compression method. It's very fast (almost as raw
	 * writing) but less size minizing comparing to Deflate method
	 * @param filePath File path
	 * @see #openDeflate(String)
	 */
	protected void openLZ4(String filePath) {
		try {
			close();
			file = new RandomAccessFile(filePath, "rw");
			fileOutputStream = new FileOutputStream(file.getFD());
			bufferedOutputStream = new BufferedOutputStream(fileOutputStream, BUFFER_SIZE);
			lz4BlockOutputStream = new LZ4BlockOutputStream(bufferedOutputStream, BUFFER_SIZE, LZ4compressor);
			objectOutputStream = new ObjectOutputStream(lz4BlockOutputStream);
			closed = false;
			deleted = false;
		} catch (IOException exception) {}
		this.filePath = filePath;
	}

	/**
	 * Open file for data writing using java Deflater for data compressing. This method is efficent
	 * for size minimizing but very slow comparing to LZ4 methode.
	 * @param filePath File path
	 * @see #openLZ4(String)
	 */
	protected void openDeflate(String filePath) {
		try {
			close();
			file = new RandomAccessFile(filePath, "rw");
			fileOutputStream = new FileOutputStream(file.getFD());
			bufferedOutputStream = new BufferedOutputStream(fileOutputStream, BUFFER_SIZE);
			compressor = new Deflater(COMPRESSION_LEVEL);
			compressorOutput = new DeflaterOutputStream(bufferedOutputStream, compressor, BUFFER_SIZE);
			objectOutputStream = new ObjectOutputStream(compressorOutput);
			closed = false;
			deleted = false;
		} catch (IOException exception) {}
		this.filePath = filePath;
	}

	/**
	 * Open file for data writing with no compression method. it is very fast but more disk
	 * resources eager
	 * @param filePath File path
	 */
	protected void openRaw(String filePath) {
		try {
			close();
			file = new RandomAccessFile(filePath, "rw");
			fileOutputStream = new FileOutputStream(file.getFD());
			bufferedOutputStream = new BufferedOutputStream(fileOutputStream, BUFFER_SIZE);
			objectOutputStream = new ObjectOutputStream(bufferedOutputStream);
			closed = false;
			deleted = false;
		} catch (IOException exception) {}
		this.filePath = filePath;
	}

	/**
	 * Close streams opened
	 */
	public void close() {
		if (closed)
			return;
		try {
			if (objectOutputStream != null)
				objectOutputStream.close();
			if (lz4BlockOutputStream != null)
				lz4BlockOutputStream.close();
			if (compressorOutput != null)
				compressorOutput.close();
			if (bufferedOutputStream != null)
				bufferedOutputStream.close();
			if (fileOutputStream != null)
				fileOutputStream.close();
			if (file != null)
				file.close();
		} catch (IOException exception) {}
		closed = true;
	}

	/**
	 * Delete from the disc file associated with this writer
	 */
	public void delete() {
		close();
		if (deleted)
			return;
		try {
			new File(filePath).delete();
		} catch (Exception exception) {}
		deleted = true;
	}
}
