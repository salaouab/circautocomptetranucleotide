package fr.unistra.model;

import static fr.unistra.utils.GraphAlgorithmConstants.tetraAndCompEdge;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class representing a tetranucleotide
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class Tetranucleotide implements Serializable {
	private static final long	  serialVersionUID	= 1L;
	private String	              tetranucleotide;
	/** List containing the tetranuclotide and its complementary edges */
	private ArrayList<StringEdge>	edgeList	   = new ArrayList<StringEdge>();

	/**
	 * Constructs the tetranucleotide and holds its and complements edge list
	 * 
	 * @param tetranucleotide Tetranucleotide
	 */
	public Tetranucleotide(String tetranucleotide) {
		this.tetranucleotide = tetranucleotide;
		tetraAndCompEdge(tetranucleotide, edgeList);
	}

	public String getTetranucleotide() {
		return tetranucleotide;
	}

	public void setTetranucleotide(String tetranucleotide) {
		this.tetranucleotide = tetranucleotide;
		edgeList.clear();
		tetraAndCompEdge(tetranucleotide, edgeList);
	}

	public ArrayList<StringEdge> getEdgeList() {
		return edgeList;
	}

	public void setEdgeList(ArrayList<StringEdge> edgeList) {
		this.edgeList = edgeList;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Tetranucleotide)
			return tetranucleotide.compareTo(((Tetranucleotide) obj).tetranucleotide) == 0;
		if (obj instanceof String)
			return tetranucleotide.compareTo((String) obj) == 0;

		return false;
	}

	@Override
	public int hashCode() {
		return tetranucleotide.hashCode();
	}

	@Override
	public String toString() {
		return tetranucleotide;
	}
}
