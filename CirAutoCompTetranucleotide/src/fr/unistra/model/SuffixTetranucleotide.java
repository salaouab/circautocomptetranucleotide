package fr.unistra.model;

import java.io.Serializable;

/**
 * Represents a suffix tetranucleotide with its exploration last place saved
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class SuffixTetranucleotide implements Serializable {
	private static final long	serialVersionUID	= 1L;
	private Tetranucleotide	  tetranucleotide;
	private int	              indexOfS_12;
	private int	              indexOfS_114;

	public SuffixTetranucleotide() {}

	public SuffixTetranucleotide(Tetranucleotide tetranucleotide, int indexOfS_12, int indexOfS_114) {
		this.tetranucleotide = tetranucleotide;
		this.indexOfS_12 = indexOfS_12;
		this.indexOfS_114 = indexOfS_114;
	}

	public SuffixTetranucleotide(Tetranucleotide tetranucleotide) {
		this(tetranucleotide, 0, 0);
	}

	public Tetranucleotide getTetranucleotide() {
		return tetranucleotide;
	}

	public void setTetranucleotide(Tetranucleotide tetranucleotide) {
		this.tetranucleotide = tetranucleotide;
	}

	public int getIndexOfS_12() {
		return indexOfS_12;
	}

	public void setIndexOfS_12(int indexOfS_12) {
		this.indexOfS_12 = indexOfS_12;
	}

	public int getIndexOfS_114() {
		return indexOfS_114;
	}

	public void setIndexOfS_114(int indexOfS_114) {
		this.indexOfS_114 = indexOfS_114;
	}

	@Override
	public String toString() {
		return tetranucleotide.toString();
	}
}
