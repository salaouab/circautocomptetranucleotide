package fr.unistra.model;

import static fr.unistra.utils.GraphAlgorithmConstants.DEBUG_MODE;

import java.io.*;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import net.jpountz.lz4.*;

/**
 * Class for serialized data sets reading in files
 * @author Abdoul-Djawadou SALAOU
 */
public class TaskObjectReader {
	private ObjectInputStream	objectInputStream;
	private BufferedInputStream	bufferedInputStream;
	private FileInputStream	    fileInputStream;
	private RandomAccessFile	file;
	private boolean	            isTaskAvailable	= true;
	private String	            filePath;
	private boolean	            closed	        = false;
	private boolean	            deleted	        = false;
	private static final int	BUFFER_SIZE	    = 419304;

	/** Java Deflator */
	private InflaterInputStream	decompressorInput;

	/** LZ4 compression: Very interesting on velocity */
	private LZ4Factory	        factory	        = LZ4Factory.fastestInstance();
	private LZ4FastDecompressor	LZ4decompressor	= factory.fastDecompressor();
	private LZ4BlockInputStream	lz4BlockInputStream;

	/**
	 * Open the file for objects reading.
	 * @param filePath File path
	 * @see #open(String)
	 */
	public TaskObjectReader(String filePath) {
		closed = true;
		open(filePath);
	}

	/**
	 * Read the object from the intput stream
	 * @return Return the object read from the stream
	 */
	public CircularComplementatryTetranucleotide readObject() {
		try {
			return (CircularComplementatryTetranucleotide) objectInputStream.readObject();
		} catch (Exception exception) {
			if (DEBUG_MODE)
				exception.printStackTrace();
			isTaskAvailable = false;
			return null;
		}
	}

	/**
	 * Open the file for data reading. Currently LZ4 decompression is used load the file size
	 * @param filePath File path
	 * @see #openLZ4(String)
	 * @see #openRaw(String)
	 * @see #openDeflate(String)
	 */
	public void open(String filePath) {
		openLZ4(filePath);
	}

	/**
	 * Open file for data reading using LZ4 decompression method.
	 * @param filePath File path
	 * @see #openDeflate(String)
	 */
	protected void openLZ4(String filePath) {
		try {
			close();
			file = new RandomAccessFile(filePath, "r");
			fileInputStream = new FileInputStream(file.getFD());
			bufferedInputStream = new BufferedInputStream(fileInputStream, BUFFER_SIZE);
			lz4BlockInputStream = new LZ4BlockInputStream(bufferedInputStream, LZ4decompressor);
			objectInputStream = new ObjectInputStream(lz4BlockInputStream);
			closed = false;
			deleted = false;
		} catch (IOException exception) {}
		this.filePath = filePath;
	}

	/**
	 * Open file for data reading using java {@link Inflater} for data decompressing.
	 * @param filePath File path
	 * @see #openLZ4(String)
	 */
	protected void open1Deflate(String filePath) {
		try {
			close();
			file = new RandomAccessFile(filePath, "r");
			fileInputStream = new FileInputStream(file.getFD());
			bufferedInputStream = new BufferedInputStream(fileInputStream, BUFFER_SIZE);
			decompressorInput = new InflaterInputStream(bufferedInputStream, new Inflater(), BUFFER_SIZE);
			objectInputStream = new ObjectInputStream(decompressorInput);
			closed = false;
			deleted = false;
		} catch (IOException exception) {}
		this.filePath = filePath;
	}

	/**
	 * Open file for raw data reading (file with no compression )
	 * @param filePath File path
	 */
	protected void openRaw(String filePath) {
		try {
			close();
			file = new RandomAccessFile(filePath, "r");
			fileInputStream = new FileInputStream(file.getFD());
			bufferedInputStream = new BufferedInputStream(fileInputStream, BUFFER_SIZE);
			objectInputStream = new ObjectInputStream(bufferedInputStream);
			closed = false;
			deleted = false;
		} catch (IOException exception) {}
		this.filePath = filePath;
	}

	/**
	 * Close streams opened
	 */
	public void close() {
		if (closed)
			return;
		try {
			if (file != null)
				file.close();
			if (fileInputStream != null)
				fileInputStream.close();
			if (bufferedInputStream != null)
				bufferedInputStream.close();
			if (decompressorInput != null)
				decompressorInput.close();
			if (lz4BlockInputStream != null)
				lz4BlockInputStream.close();
			if (objectInputStream != null)
				objectInputStream.close();
		} catch (IOException exception) {}
		closed = true;
	}

	/**
	 * Delete from the disc file associated with this reader
	 */
	public void delete() {
		close();
		if (deleted)
			return;
		try {
			new File(filePath).delete();
		} catch (Exception exception) {}
		deleted = true;
	}

	public boolean isTaskAvailabe() {
		return isTaskAvailable;
	}
}
