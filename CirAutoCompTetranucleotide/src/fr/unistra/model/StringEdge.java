package fr.unistra.model;

import java.io.Serializable;

import org.jgraph.graph.DefaultEdge;
import org.jgrapht.DirectedGraph;

/**
 * Represent and directed edge with its source and target nodes.
 * @author Abdoul-Djawadou SALAOU
 */
public class StringEdge implements Serializable {
	private static final long	  serialVersionUID	= 1L;
	private Node	              sourceNode;
	private Node	              targetNode;
	private transient DefaultEdge	edge;

	// private Edge
	public StringEdge() {}

	public StringEdge(String source, String target) {
		sourceNode = new Node(source);
		targetNode = new Node(target);
	}

	public StringEdge(Node source, Node target) {
		sourceNode = source;
		targetNode = target;
	}

	public synchronized DefaultEdge addTo(DirectedGraph<Node, DefaultEdge> graph) {
		if (graph == null)
			return null;
		graph.addVertex(sourceNode);
		graph.addVertex(targetNode);
		edge = graph.addEdge(sourceNode, targetNode);
		return edge;
	}

	public void removeFrom(DirectedGraph<Node, DefaultEdge> graph) {
		if (graph == null)
			return;
		graph.removeEdge(edge);
	}

	public Node getSourceNode() {
		return sourceNode;
	}

	public void setSourceNode(Node sourceNode) {
		this.sourceNode = sourceNode;
	}

	public Node getTargetNode() {
		return targetNode;
	}

	public void setTargetNode(Node targetNode) {
		this.targetNode = targetNode;
	}

	@Override
	public String toString() {
		return sourceNode + "-->" + targetNode;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof StringEdge)
			return sourceNode.equals(((StringEdge) obj).sourceNode) && targetNode.equals(((StringEdge) obj).targetNode);
		return false;
	}
}
