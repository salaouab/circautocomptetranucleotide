package fr.unistra.model;

import java.io.Serializable;

/**
 * Class representing a node with its value
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class Node implements Serializable {
	private static final long	serialVersionUID	= 1L;
	private String	          value;

	public Node(String node) {
		value = node;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Node)
			return value.compareTo(((Node) obj).value) == 0;
		return false;
	}

	@Override
	public String toString() {
		return value;
	}

	@Override
	public int hashCode() {
		return value.hashCode();
	}
}
