package fr.unistra.controller;

import static fr.unistra.utils.GraphAlgorithmConstants.INTERUPTOR_FLAG;
import static fr.unistra.utils.GraphAlgorithmConstants.MAX_SELF_COMPLEMENTARY;

import java.util.ArrayList;

import org.jgraph.graph.DefaultEdge;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.SimpleDirectedGraph;

import fr.unistra.model.*;

/**
 * Performs the task fetching from the {@link TaskManager }. Concretly it get task from tasks
 * manager, execute it; when done fetch another to process.
 * 
 * @author Abdoul-Djawadou SALAOU
 */
public class TaskExecutor implements Runnable {
	// variables for task management
	private TaskManager	                           taskManager;
	private CircularComplementatryTetranucleotide	tetraBase;
	private ArrayList<Tetranucleotide>	           searchSet;
	private int	                                   searchSetSize;
	private int	                                   startIndex;
	private int	                                   indexS_12;
	private int	                                   indexS_114;
	public int	                                   executorId;
	private static int	                           executorNumber	= 0;
	private boolean	                               isAutoCompSet;
	private ArrayList<DefaultEdge>	               lastAddEdgeList	= new ArrayList<DefaultEdge>();
	private Tetranucleotide	                       currentTetranucleotide;
	// graph things :)
	private SimpleDirectedGraph<Node, DefaultEdge>	graph;
	private CycleDetector<Node, DefaultEdge>	   graphCycleDetector;

	/**
	 * Contructor with the tasks manager informed
	 * 
	 * @param taskManager : The tasks manager
	 */
	public TaskExecutor(TaskManager taskManager) {
		this.taskManager = taskManager;
		executorNumber++;
		// Id of this thread
		executorId = executorNumber;
	}

	/**
	 * Add string edge and its nodes to the graph
	 * 
	 * @param tetranucleotide : Tetranucleotide's edges to add to the graph
	 */
	public void addTetranucleotideEdge(Tetranucleotide tetranucleotide) {
		for (StringEdge edge : tetranucleotide.getEdgeList())
			lastAddEdgeList.add(edge.addTo(graph));
	}

	/**
	 * Removes the last edges add to the graph
	 */
	public void removeLastAddEdges() {
		graph.removeAllEdges(lastAddEdgeList);
		lastAddEdgeList.clear();
	}

	/**
	 * Set up the graph
	 */
	public void initGraph() {
		graph = new SimpleDirectedGraph<Node, DefaultEdge>(DefaultEdge.class);
		for (Tetranucleotide tetranucleotide : tetraBase.getComplementaryTetranucleotideSet())
			addTetranucleotideEdge(tetranucleotide);

		lastAddEdgeList.clear();
		return;
	}

	/**
	 * Check if the set worth processing and configure some variables
	 * 
	 * @return Return true if the set is good for processing , false otherwise
	 */
	public boolean checkAndSetup() {
		isAutoCompSet = tetraBase.isUsingAutoComplementarySet();
		if (isAutoCompSet) {
			if (tetraBase.getNumberOfSelfComplementary() == MAX_SELF_COMPLEMENTARY)
				return false;
			tetraBase.incNumberOfSelfComplementary();
		}
		searchSet = tetraBase.getSearchSet();
		searchSetSize = searchSet.size();
		startIndex = tetraBase.getStartIndex();
		indexS_12 = tetraBase.getIndexOfS_12();
		indexS_114 = tetraBase.getIndexOfS_114();
		return startIndex < searchSetSize;
	}

	/**
	 * Detect if the current Tetranucleotide is already marked as cycle introductor or introduce in
	 * this set.
	 * 
	 * @return
	 */
	public boolean isCyclicTetranucleotide() {
		if (tetraBase.isCyclicTetranucleotide(currentTetranucleotide))
			return true;

		boolean jugement;
		addTetranucleotideEdge(currentTetranucleotide);
		graphCycleDetector = new CycleDetector<Node, DefaultEdge>(graph);
		jugement = graphCycleDetector.detectCycles();

		// unstack the last edges
		removeLastAddEdges();
		return jugement;
	}

	/**
	 * Perform the search over tetranucleotide set
	 */
	public void processTetranucleotideSearch() {
		if (!checkAndSetup())
			return;

		initGraph();
		for (int i = startIndex; i < searchSetSize; i++) {
			currentTetranucleotide = searchSet.get(i);
			if (isCyclicTetranucleotide())
				tetraBase.addCyclicTetranucleotide(currentTetranucleotide);
			else {
				if (isAutoCompSet)
					tetraBase.addSuffixTetranucleotide(new SuffixTetranucleotide(currentTetranucleotide, i + 1, indexS_114));
				else
					tetraBase.addSuffixTetranucleotide(new SuffixTetranucleotide(currentTetranucleotide, indexS_12, i + 1));
			}
		}
	}

	/**
	 * This is the main methode performing tasks: Get task, Process it, Add the result, Notify the
	 * task Manager And loop through this until the job is done .
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		do {
			synchronized (taskManager) {
				if (!taskManager.isTaskAvailable()) {
					taskManager.waitForTask();
				}
			}
			if (taskManager.isJobDone())
				break;

			try {
				tetraBase = taskManager.getTask();
			} catch (Exception exception) {
				continue;
			}
			processTetranucleotideSearch();
			if (!tetraBase.isSuffixListEmpty())
				taskManager.addResult(tetraBase);
			taskManager.taskDone();

		} while (!INTERUPTOR_FLAG && !taskManager.isJobDone());

	}
}
